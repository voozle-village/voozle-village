#ifndef _GAMETIME_H_
#define _GAMETIME_H_

#include "fps.h"

#include <string>

class GameTime
{
public:
  static float gameTimePerRealTime();

  static GameTime Frame();

  static GameTime Frames(const int value);
  static GameTime Seconds(const int value);
  static GameTime Minutes(const float value);
  static GameTime Hours(const float value);
  static GameTime Days(const float value);

  GameTime();

  GameTime(const GameTime&) = default;
  GameTime& operator=(const GameTime&) = default;

  GameTime operator+(const GameTime& that) const;
  GameTime& operator+=(const GameTime& that);
  GameTime operator-(const GameTime& that) const;
  GameTime& operator-=(const GameTime& that);

  bool operator==(const GameTime& that) const;
  bool operator!=(const GameTime& that) const;
  bool operator>(const GameTime& that) const;
  bool operator>=(const GameTime& that) const;
  bool operator<(const GameTime& that) const;
  bool operator<=(const GameTime& that) const;

  int getHours() const;
  int getMinutes() const;
  int getSeconds() const;
  int getValue() const;
  
  std::string getStringHM() const; 
  std::string getStringHMS() const; 

private:
  GameTime(const int seconds);

  int seconds_;
};

class GameClock
{
public:
  void tick();

  void setTime(const GameTime& time);
  const GameTime& getTime() const;

private:
  GameTime gameTime_;
};

#endif /* _GAMETIME_H_ */
