#ifndef _FONT_H_
#define _FONT_H_

#include "glyph.h"
#include "stringutils.h"

#include <SDL.h>
#include <SDL_ttf.h>

#include <vector>

#if defined(putc)
#undef putc
#endif

class Char;

class Font
{
public:

  enum Style
  {
    Normal,
    Bold,
    MaxStyle = Bold
  };

  enum Color
  {
    White,
    Grey,
    DarkGrey,
    Red,
    DarkRed,
    Green,
    DarkGreen,
    Yellow,
    DarkYellow,
    Blue,
    DarkBlue,
    MaxColor = DarkBlue
  };

  static const SDL_Color* getColor(const Color color);

  Font(SDL_Surface* screen, TTF_Font* font);

  void print( SDL_Surface* screen,
	      const int x,
	      const int y,
	      const Style style,
	      const Color color,
	      const char* text ) const;

  void putc( SDL_Surface* screen, 
	     const int x,
	     const int y,
	     const Char& c ) const;

  int getHeight() const
  {
    return height;
  }

  int getAdvance() const
  {
    return advance;
  }

private:
  Font(const Font&) {}
  Font& operator=(const Font&) { return *this; }
  
  Glyph glyphs[MaxStyle + 1][MaxColor + 1][127 - 32];

  const int ascent = 0;
  const int height = 0;
  int advance = 0;
};

enum CtrlByte {
  CtrlStyle = 1,
  CtrlColor = 2,
  CtrlEnd = 3,
};

class CtrlStack
{
public:
  CtrlStack(const Font::Style style, const Font::Color color);
  Font::Style getStyle() const;
  Font::Color getColor() const;
  void handleCtrl(const char*& ptr);
  
private:
  std::vector<Font::Style> styles_;
  std::vector<Font::Color> colors_;
  std::vector<CtrlByte> ctrls_;
};

bool isCtrl(const char* ptr);
std::string stripCtrl(std::string s);
std::string ctrl(const Font::Style style);
std::string ctrl(const Font::Style style, const std::string& text);
std::string ctrl(const Font::Color color);
std::string ctrl(const Font::Color color, const std::string& text);
std::string endCtrl();

#endif /* _FONT_H_ */
