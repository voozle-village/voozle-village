#ifndef _WORLDCONTROL_H_
#define _WORLDCONTROL_H_

#include "control.h"
#include "cursor.h"
#include "viewport.h"

#include <vpad.h>

class Screen;
class World;

void renderEntities(World* world, Viewport& viewport);

class WorldControl : public Control
{
public:
  WorldControl(ControlStack* stack, World* world, Screen* screen)
    : Control(stack)
    , world_(world)
    , screen_(screen)
    , padRepeater_(2)
    , viewport_(screen, world)
    , cursor_(world)
  {
  }
  
  void render(Screen* control) override;
  void update(vpad::BufferedPad* pad) override;

  void onEnter() override;
  void onLeave() override;
  
private:
  void gameMenu();
  void buildMenu();

  World* world_;
  Screen* screen_;
  vpad::Repeater padRepeater_;
  Viewport viewport_;
  Cursor cursor_;
  int ticks_ = 0;
};

#endif /* _WORLDCONTROL_H_ */
