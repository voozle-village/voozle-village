#include "gzpipe.h"

namespace gzpipe
{
  Writer::~Writer()
  {
    if( fp_ ) {
      fclose( fp_ );
    }
  }

  FILE* Writer::open(const std::string& filename)
  {
    if( fp_ ) {
      fclose( fp_ );
      fp_ = nullptr;
    }

    const std::string gzip = "/bin/gzip";
    fp_ = popen( (gzip + " -c > '" + filename + "'").c_str(), "w" );
    return fp_;
  }

  Reader::~Reader()
  {
    if( fp_ ) {
      fclose( fp_ );
    }
  }

  FILE* Reader::open(const std::string& filename)
  {
    if( fp_ ) {
      fclose( fp_ );
      fp_ = nullptr;
    }

    const std::string gzip = "/bin/gzip";
    fp_ = popen( (gzip + " -c -d < '" + filename + "'").c_str(), "r" );
    return fp_;
  }
  
} // ns gzpipe
