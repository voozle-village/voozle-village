#include "buildorders.h"
#include "components.h"
#include "entity.h"
#include "tile.h"
#include "tiletypes.h"
#include "world.h"

#include <iostream>

static void cancelBuild(BuildOrders::Order& order)
{
  if( order.reservedFor ) {
    auto beh = order.reservedFor->getComponent<components::Behaviour>();
    if( beh ) {
      beh->cancelActions();
    }
    order.reservedFor = nullptr;
  }
}

void BuildOrders::update(const BuildOrders& that)
{
  for( auto& p : orders_.getMap() ) {
    auto it = that.getOrders().getMap().find(p.first);
    if( it == that.getOrders().getMap().end() ){
      // Build was removed
      cancelBuild(p.second);
    }
    else if( p.second.type != (*it).second.type || p.second.reservedFor != (*it).second.reservedFor ) {
      // Build has changed
      cancelBuild(p.second);
    }
  }

  orders_ = that.getOrders();
}

BuildOrderReader::BuildOrderReader(World* world)
  : world(world)
{
}

bool BuildOrderReader::onNamedValue(const std::string& name, const std::string& value)
{
  bool ok;
  if( name == "col" ) {
    col = toInt(value, ok);
    if( !ok || col < 0 ) {
      std::cerr << "ERROR: BuildOrderReader: Invalid col '" << value << "'" << std::endl;
      return false;
    }
  }
  else if( name == "row" ) {
    row = toInt(value, ok);
    if( !ok || row < 0 ) {
      std::cerr << "ERROR: BuildOrderReader: Invalid row '" << value << "'" << std::endl;
      return false;
    }
  }
  else if( name == "order_type_id" ) {
    order.type = std::shared_ptr<OrderType>(OrderTypeFactory::instance().createInstance(value));
    if( !order.type ) {
      std::cerr << "ERROR: BuildOrderReader: Could not instantiate order type '" << value << "'" << std::endl;
      return false;
    }
    std::unique_ptr<json::ObjectReader> tmp(order.type->createReader());
    typeReader.swap(tmp);
  }
  else if( typeReader ) {
    return typeReader->onNamedValue(name, value);
  }
  else {
    std::cerr << "WARNING: BuildOrderReader: Ignoring named value '" << name << "'" << std::endl;
  }

  return true;
}

json::ArrayReader* BuildOrderReader::onNamedArray(const std::string& name)
{
  if( !typeReader ) {
    std::cerr << "ERROR: BuildOrderReader: Unexpected named array" << std::endl;
    return nullptr;
  }
  else {
    return typeReader->onNamedArray(name);
  }
}

json::ObjectReader* BuildOrderReader::onNamedObject(const std::string& name)
{
  if( !typeReader ) {
    std::cerr << "ERROR: BuildOrderReader: Unexpected named array" << std::endl;
    return nullptr;
  }
  else {
    return typeReader->onNamedObject(name);
  }
}

bool BuildOrderReader::onComplete()
{
  if( !order.type ) {
    std::cerr << "ERROR: BuildOrderReader: Incomplete" << std::endl;
    return false;
  }
  else if( !typeReader->onComplete() ) {
    return false;
  }
  else {
    world->buildOrders.setOrder(col, row, order);
    return true;
  }
}

REGISTER_ORDER_TYPE(BuildTileOrder);

BuildTileOrder::BuildTileOrder(const TileType* tileType)
  : tileType_(tileType)
{
}

std::string BuildTileOrder::getId() const
{
  return Id();
}

std::string BuildTileOrder::getDescription() const
{
  return "Build: " + tileType_->description;
} 

Char BuildTileOrder::getRepresentation() const
{
  return tileType_->c;
}

bool BuildTileOrder::isFeasible(const World* world, const int col, const int row) const
{
  const Tile& tile = world->getTile(col, row);
  return tile.type && tile.type->can_be_built_upon;
}

void BuildTileOrder::save(json::Writer& w)
{
  w.write("tile_type_id", tileType_->get_id());
}

namespace 
{
  class BuildTileOrderReader : public json::SkippingObjectReader
  {
  public:
    BuildTileOrderReader(BuildTileOrder* type)
      : type(type)
    {
    }
    
    bool onNamedValue(const std::string& name, const std::string& value) override
    {
      if( name == "tile_type_id" ) {
	const TileType* tileType = TileTypeRegistry::instance().getItem(value);
	if( !tileType ) {
	  std::cerr << "ERROR: BuildTileOrderReader: Could not instatiate tile type '" << value << "'" << std::endl;
	  return false;
	}
	type->setTileType(tileType);
      }
      else {

	return SkippingObjectReader::onNamedValue(name, value);
      }

      return true;
    }

    bool onComplete() override
    {
      const bool ok = type->getTileType() != nullptr;
      if( !ok ) {
	std::cerr << "ERROR: BuildTileOrderReader: Could not read order" << std::endl;
      }
      return ok;
    }

  private:
    BuildTileOrder* type;
  };
} // ns 

json::ObjectReader* BuildTileOrder::createReader()
{
  return new BuildTileOrderReader(this);
}

