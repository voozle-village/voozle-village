#include "astar.h"

#include <algorithm>

namespace astar
{
  typedef std::set<Node> NodeSet;
  typedef std::map<Node, float> ValueMap;
  typedef std::map<Node, Node> PathMap;

  static Path reconstructPath(const PathMap& cameFrom, const Node& current)
  {
    if( cameFrom.find(current) != cameFrom.end() ) {
      auto path = reconstructPath(cameFrom, (*cameFrom.find(current)).second);
      path.push_back(current);
      return path;
    }
    else {
      Path path;
      path.push_back(current);
      return path;
    }
  }

  Path astar( const Node& start,
	      const Node& goal,
	      ValueFunction heuristicFunction,
	      NeighbourFunction neighbourFunction,
	      ValueFunction distanceFunction )
  {
    NodeSet closedSet;
    NodeSet openSet { start };
    ValueMap gScore{ {start, 0.0f } };
    ValueMap fScore{ {start, heuristicFunction(start, goal)} };
    PathMap cameFrom;
    
    while( !openSet.empty() ) {
      auto current = *std::min_element( openSet.begin(),
					openSet.end(),
					[&fScore](const Node& a, const Node& b) {
					  return fScore[a] < fScore[b];
					} );

      if( current == goal ) {
	return reconstructPath(cameFrom, goal);
      }
  
      openSet.erase(current);
      closedSet.insert(current);

      for( const auto neighbour : neighbourFunction(current) ) {
	if( closedSet.find(neighbour) != closedSet.end() ) {
	  continue;
	}
	else {
	  const auto tentativeGScore = gScore[current] + distanceFunction(current, neighbour);
	  if( openSet.find(neighbour) == openSet.end()
	      || tentativeGScore < gScore[neighbour] ) {
	    cameFrom[neighbour] = current;
	    gScore[neighbour] = tentativeGScore;
	    fScore[neighbour] = gScore[neighbour] + heuristicFunction(neighbour, goal);
	    if( openSet.find(neighbour) == openSet.end() ) {
	      openSet.insert(neighbour);
	    }
	  }
	}
      }
    }
  
    return Path();
  }

} // ns astar
