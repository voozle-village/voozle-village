#include "entitytypeproperties.h"

namespace entitytypeproperties
{
  BuildRequirements::BuildRequirements(EntityType* entityType)
    : EntityType::Property(entityType)
  {
  }
} // ns entitytypeproperties
