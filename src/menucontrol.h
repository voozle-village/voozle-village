#ifndef _MENUCONTROL_H_
#define _MENUCONTROL_H_

#include "control.h"

#include <functional>
#include <string>
#include <vector>

struct MenuOption
{
  std::string title;
  std::string description;
  bool enabled;
};

class MenuControl : public Control
{
public:
  typedef std::function<void(MenuControl*, int)> Callback;

  static void cancelMenu(MenuControl* mc, int);

  MenuControl(ControlStack* stack, const std::string& title, const std::vector<MenuOption>& options, 
	      Callback callback, Callback cancel = [](MenuControl*, int){} );
  
  void render(Screen* screen) override;
  void update(vpad::BufferedPad* pad) override;
  void onEnter() override;

private:
  const std::string title_;
  const std::vector<MenuOption> options_;
  Callback callback_;
  Callback cancel_;
  int cursor_row_ = 0;
  int scroll_row_ = 0;
  int max_title_length_ = 0;
};

#endif /* _MENUCONTROL_H_ */
