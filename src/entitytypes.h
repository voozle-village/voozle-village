#ifndef _ENTITYTYPES_H_
#define _ENTITYTYPES_H_

#include "registry.h"
#include "singleton.h"

#include <string>
#include <vector>

class Entity;
class World;

class EntityType 
{
public:
  class Property
  {
  public:
    Property(EntityType* entityType);
    Property(const Property&) = delete;
    virtual ~Property();
    Property& operator=(const Property&) = delete;
  };

  EntityType(const EntityType& that) = delete;
  virtual ~EntityType();
  EntityType& operator=(const EntityType& that) = delete;

  template<class P>
  P* getProperty() const {
    for( auto up : properties_ ) {
      P* down = dynamic_cast<P*>(up);
      if( down ) {
	return down;
      }
    }
    return nullptr;
  }

  virtual std::string getId() const = 0;
  virtual Entity* createInstance(World* world, const int id) const = 0;

protected:
  EntityType() = default;

private:
  std::vector<Property*> properties_;
};

template<>
std::string getId(const EntityType* entityType);

class EntityTypeRegistry : public Registry<EntityType>
{
public:
  template<class P>
  std::vector<const EntityType*> getItemsByProperty()
  {
    std::vector<const EntityType*> result;
    for( const auto& p : getItemMap() ) {
      if( p.second->getProperty<P>() ) {
	result.push_back(p.second);
      }
    }
    return result;
  }
};

template<class Child>
struct EntityTypeBase : public SingletonBase<Child, EntityType> {};

#define REGISTER_ENTITY_TYPE(cls) EntityTypeRegistry::Registrator<cls> __register##cls

namespace entitytypes
{
  class Null : public EntityTypeBase<Null> 
  {
    std::string getId() const override;
    Entity* createInstance(World* world, const int id) const override;
  };

  struct WoodLog : public EntityTypeBase<WoodLog> 
  {
    std::string getId() const override;
    Entity* createInstance(World* world, const int id) const override;
  };

  struct StoneBlock : public EntityTypeBase<StoneBlock> 
  {
    std::string getId() const override;
    Entity* createInstance(World* world, const int id) const override;
  };

} // ns entitytypes

#endif /* _ENTITYTYPES_H_ */
