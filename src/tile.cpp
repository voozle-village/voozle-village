#include "tile.h"
#include "world.h"

#include <iostream>

Tile::Tile(const TileType* type, const GameTime& time)
  : type(type)
  , time(time)
{
}

void Tile::save(json::Writer& w)
{
  w.startObject();
  w.write("type_id", type->get_id());
  w.write("time", time.getValue());
  w.endObject();
}

TileReader::TileReader(World* world, const int col, const int row)
  : world(world)
  , col(col)
  , row(row)
{
}

bool TileReader::onNamedValue(const std::string& name, const std::string& value)
{
  if( name == "type_id" ) {
    tile.type = TileTypeRegistry::instance().getItem(value);
    if( !tile.type ) {
      std::cerr << "ERROR: Could not instantiate tile type '" << value << "'" << std::endl;
      return false;
    }
  }
  else if( name == "time" ) {
    bool ok;
    const int seconds = toInt(value, ok);
    if( !ok || seconds < 0 ) {
      std::cerr << "WARNING: TileReader: Could not read time" << std::endl;
    }
    tile.time = GameTime::Seconds(seconds);
  }
  else {
    std::cerr << "WARNING: TileReader: Ignoring value: " << name << std::endl;
  }
  return true;
}

json::ArrayReader* TileReader::onNamedArray(const std::string& name)
{
  std::cerr << "WARNING: TileReader: Skipping array '" << name << "'" << std::endl;
  return new json::SkippingArrayReader;
}

json::ObjectReader* TileReader::onNamedObject(const std::string& name)
{
  std::cerr << "WARNING: TileReader: Skipping object '" << name << "'" << std::endl;
  return new json::SkippingObjectReader;
}

bool TileReader::onComplete()
{
  const bool ok = tile.type && tile.type != tiletypes::Null::instance();
  if( ok ) {
    world->setTile(col, row, tile);
  }
  else {
    std::cerr << "ERROR: TileReader: Could not read tile" << std::endl;
  }
  return ok;
}
