#ifndef _SINGLETON_H_
#define _SINGLETON_H_

template<class Child, class Base>
class SingletonBase : public Base
{
public:
  static Child* instance() 
  { 
    static Child t;
    return &t;
  }
};

#endif /* _SINGLETON_H_ */
