#ifndef _WORLD_H_
#define _WORLD_H_

#include "buildorders.h"
#include "entity.h"
#include "gametime.h"
#include "log.h"
#include "names.h"
#include "tile.h"

#include <jsonwriter.h>

#include <string>
#include <vector>

class Entity;
class Screen;

class TileUpdateWindow
{
public:
  TileUpdateWindow(World* world);
  void update();

private:
  enum {
    NumCols = 100,
    NumRows = 75
  };

  World* world_;
  const GameTime tick_;
  int col_ = 0;
  int row_ = 0;
};

class  World
{
public:
  enum {
    DefaultNumCols = 40,
    DefaultNumRows = (DefaultNumCols * 3) / 4,
  };

  enum {
    FormatVersion = 2,
  };

  static World* load(const std::string& filename);
  static World* generate();

  World(const World&) = delete;
  World& operator=(const World&) = delete;
  ~World();

  int getNumCols() const
  {
    return numCols_;
  }

  int getNumRows() const
  {
    return numRows_;
  }

  const Tile& getTile(const int col, const int row) const;
  void setTile(const int col, const int row, const Tile& tile);

  void save(json::Writer& writer);
  bool save(const std::string& filename);

  void update();

  const std::vector<Entity*>& getEntities() const
  {
    return entities_;
  }

  std::vector<Entity*> getEntitiesAt(const int col, const int row) const;
  Entity* getTopEntityAt(const int col, const int row) const;
  void addEntity(Entity* ent);
  void addEntityRef(const EntityPointerRef& ref);
  void addEntityRef(const EntityPointerVectorRef& ref);

  bool canMoveTo(const Entity* ent, const int col, const int row) const;

  void initViewport(Screen* screen);

  int createNewEntityId();

  BuildOrders buildOrders;
  GameClock clock;
  Log log;

  int viewportCol = 0;
  int viewportRow = 0;
  int cursorCol = 0;
  int cursorRow = 0;

  NameGenerator nameGenerator;

  World(const int numCols = DefaultNumCols, const int numRows = DefaultNumRows);
  bool resolveEntityIdRefs();

private:
  friend class TileUpdateWindow;
  friend class WorldReader;

  const int numCols_ = 0;
  const int numRows_ = 0;
  Tile* tiles_ = 0;

  std::vector<Entity*> entities_;
  std::map<int, Entity*> entitiesById_;
  std::vector<EntityPointerRef> entityPointerRefs_;
  std::vector<EntityPointerVectorRef> entityPointerVectorRefs_;
  int maxEntityId_ = 0;

  TileUpdateWindow tileUpdateWindow_;
};

#endif /* _WORLD_H_ */
