#ifndef _BUILDCONTROL_H_
#define _BUILDCONTROL_H_

#include "buildorders.h"
#include "control.h"
#include "cursor.h"
#include "viewport.h"

#include <vpad.h>

class Screen;
class World;

void renderBuildOrders(const BuildOrders& buildOrders, Viewport& viewport);

class BuildControl : public Control
{
public:
  BuildControl(ControlStack* stack, World* world, Screen* screen, const BuildOrders::Order& order);
  
  void render(Screen* control) override;
  void update(vpad::BufferedPad* pad) override;

  void onEnter() override;
  void onLeave() override;
  
private:
  enum Mode {
    ModeBuild,
    ModeErase,
  };

  World* world_;
  vpad::Repeater padRepeater_;

  int ticks_ = 0;
  BuildOrders buildOrders_;
  BuildOrders::Order order_;
  Viewport viewport_;
  Cursor cursor_;
  Mode mode_ = ModeBuild;
};

#endif /* _BUILDCONTROL_H_ */
