#include "components.h"
#include "entity.h"
#include "names.h"
#include "world.h"

#include <iostream>
#include <string>

Component::Component(Entity* entity)
  : entity_(entity)
{
}

Component::~Component()
{
}

void Component::save(json::Writer& /*w*/)
{
}

json::ObjectReader* Component::createReader()
{
  return new json::SkippingObjectReader;
}

void Component::update()
{
}

namespace components
{
  MapPosition::MapPosition(Entity* entity)
    : Component(entity)
  {
  }
  
  int MapPosition::distSquared(const MapPosition& that) const
  {
    const int dCol = that.col - col;
    const int dRow = that.row - row;
    return dCol * dCol + dRow * dRow;
  }
  
  void MapPosition::save(json::Writer& w)
  {
    w.write("col", col);
    w.write("row", row);
  }
  
  namespace 
  {
    class MapPositionReader : public json::SkippingObjectReader
    {
    public:
      MapPositionReader(MapPosition* pos)
	: pos(pos)
      {
      }
      
      bool onNamedValue(const std::string& name, const std::string& value) override
      {
	bool ok;
	if( name == "col" ) {
	  pos->col = toInt(value, ok);
	  if( !ok || pos->col < 0 ) {
	    std::cerr << "ERROR: MapPositionReader: Invalid col = '" << value << "'" << std::endl;
	    return false;
	  }
	}
	else if( name == "row" ) {
	  pos->row = toInt(value, ok);
	  if( !ok || pos->row < 0 ) {
	    std::cerr << "ERROR: MapPositionReader: Invalid row = '" << value << "'" << std::endl;
	    return false;
	  }
	}
	else {
	  return SkippingObjectReader::onNamedValue(name, value);
	}

	return true;
      }
      
    private:
      MapPosition* pos;
    };
  } // ns 

  json::ObjectReader* MapPosition::createReader()
  {
    return new MapPositionReader(this);
  }

  std::string MapPosition::getTypeId() const
  {
    return "map_position";
  }

  MapRepresentation::MapRepresentation(Entity* entity)
    : Component(entity)
  {
  }

  void MapRepresentation::save(json::Writer& w)
  {
    w.write("visible", visible);
  }

  namespace 
  {
    class MapRepresentationReader : public json::SkippingObjectReader
    {
    public:
      MapRepresentationReader(MapRepresentation* rep)
	: rep(rep)
      {
      }
      
      bool onNamedValue(const std::string& name, const std::string& value) {
	if( name == "visible" ) {
	  bool ok;
	  rep->visible = toBool(value, ok);
	  if( !ok ) {
	    std::cerr << "ERROR: MapRepresentationReader: Could not read 'visible'" << std::endl;
	    return false;
	  }
	}
	else {
	  return SkippingObjectReader::onNamedValue(name, value);
	}

	return true;
      }
      
    private:
      MapRepresentation* rep;
    };
  } // ns 

  json::ObjectReader* MapRepresentation::createReader()
  {
    return new MapRepresentationReader(this);
  }

  std::string MapRepresentation::getTypeId() const
  {
    return "map_representation";
  }

  Reservation::Reservation(Entity* ent)
    : Component(ent)
  {
  }

  void Reservation::save(json::Writer& w)
  {
    w.write("entity_id", entity
		              ? entity->getId()
		              : 0 );
  }

  class ReservationReader : public json::SkippingObjectReader
  {
  public:
    ReservationReader(Reservation* res)
      : res(res)
    {
    }

    bool onNamedValue(const std::string& name, const std::string& value) override
    {
      if( name == "entity_id" ) {
	bool ok;
	const int id = toInt(value, ok);
	if( !ok || id < 0 ) {
	  std::cerr << "ERROR: ReservationReader: Could not read entity_id '" << value << "'" << std::endl;
	  return false;
	}
	if( id > 0 ) {
	  res->getEntity()->getWorld()->addEntityRef({id, res->entity});
	}
      }
      else {
	return SkippingObjectReader::onNamedValue(name, value);
      }
      return true;
    }
    
  private:
    Reservation* res;
  };

  json::ObjectReader* Reservation::createReader()
  {
    return new ReservationReader(this);
  }

  std::string Reservation::getTypeId() const
  {
    return "reservation";
  }

  Behaviour::Behaviour(Entity* entity)
    : Component(entity)
  {
  }
  
  Behaviour::~Behaviour()
  {
    cancelActions();
  }

  void Behaviour::update()
  {
    if( !currentAction() ) {
      cancelActions();
      think();
    }

    Entity* ent = getEntity();
    World* world = ent->getWorld();

    Action* act = currentAction();
    if( act ) {
      switch( act->update(ent) ) {
      case Action::Finished:
	nextAction();
	if( !currentAction() ) {
	  world->log.addSays(ent, "All done!");
	}
	break;
      case Action::InProgress:
	break;
      case Action::Cancelled:
	world->log.addSays(ent, "I'm giving up!");
	cancelActions();
	break;
      }
    }
  }

  std::string Behaviour::getTypeId() const
  {
    return "behaviour";
  }
  
  void Behaviour::addAction(Action* action)
  {
    actions_.push_back(action);
  }

  void Behaviour::cancelActions()
  {
    for( auto a : actions_ ) {
      a->cancel(getEntity());
    }
    deleteActions();
  }

  Behaviour::Action* Behaviour::currentAction() const
  {
    return (index_ < actions_.size())
      ? actions_[index_]
      : nullptr;
  }

  Behaviour::Action* Behaviour::nextAction()
  {
    index_++;
    return currentAction();
  }
  
  void Behaviour::deleteActions()
  {
    for( auto a : actions_ ) {
      delete a;
    }
    actions_.clear();
    index_ = 0;
  }

  Name::Name(Entity* entity)
    : Component(entity)
  {
  }

  void Name::save(json::Writer& w)
  {
    w.write("name", name);
  }

  namespace 
  {
    class NameReader : public json::SkippingObjectReader
    {
    public:
      NameReader(Name* name)
	: name(name)
      {
      }

      bool onNamedValue(const std::string& name, const std::string& value) override
      {
	if( name == "name" ) {
	  this->name->getEntity()->getWorld()->nameGenerator.addName(value);
	  this->name->name = value;
	}
	else {
	  return SkippingObjectReader::onNamedValue(name, value);
	}
	return true;
      }

      bool onComplete() override
      {
	const bool ok = !name->name.empty() && name->name != "n/a";
	if( !ok ) {
	  std::cerr << "ERROR: NameReader: Could not read name" << std::endl;
	}
	return ok;
      }

    private:
      Name* name;
    };
  } // ns 

  json::ObjectReader* Name::createReader()
  {
    return new NameReader(this);
  }

  std::string Name::getTypeId() const
  {
    return "name";
  }

  Sex::Sex(Entity* entity)
    : Component(entity)
  {
  }
  
  void Sex::save(json::Writer& w)
  {
    w.write("value", value);
  }

  namespace 
  {
    class SexReader : public json::SkippingObjectReader
    {
    public:
      SexReader(Sex* sex)
	: sex(sex)
      {
      }
      
      bool onNamedValue(const std::string& name, const std::string& value) override
      {
	if( name == "value" ) {
	  bool ok;
	  const int tmp = toInt(value, ok);
	  if( !ok || (tmp != Sex::Male && tmp != Sex::Female) ) {
	    std::cerr << "ERROR: SexReader: Could not read value '" << value << "'" << std::endl;
	    return false;
	  }
	  sex->value = static_cast<Sex::Value>(tmp);
	}
	else {
	  return SkippingObjectReader::onNamedValue(name, value);
	}
	return true;
      }
      
    private:
      Sex* sex;
    };
  } // ns 

  json::ObjectReader* Sex::createReader()
  {
    return new SexReader(this);
  }
  
  std::string Sex::getTypeId() const
  {
    return "sex";
  }

} // ns components
