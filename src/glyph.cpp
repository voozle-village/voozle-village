#include "glyph.h"

Glyph::Glyph()
  : minx(0)
  , maxy(0)
  , surface(0)
{
}

Glyph::~Glyph()
{
  if( surface ) {
    SDL_FreeSurface(surface);
  }
}

void Glyph::render(SDL_Surface* screen, const int x, const int y, const int ascent) const
{
  if( surface ) {
    SDL_Rect rect {
      (Sint16) (x + minx),
	(Sint16) (y + ascent - maxy),
	(Uint16) surface->w,
	(Uint16) surface->h
	};
    SDL_BlitSurface(surface, 0, screen, &rect);
  }
}
