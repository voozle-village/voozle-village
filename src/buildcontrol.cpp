#include "buildcontrol.h"
#include "controlstack.h"
#include "font.h"
#include "screen.h"
#include "world.h"
#include "worldcontrol.h"

#include <vpad.h>

#include <iomanip>
#include <sstream>

void renderBuildOrders(const BuildOrders& buildOrders, Viewport& viewport)
{
    for( const auto& p : buildOrders.getOrders().getMap() ) {
      
      const int col = p.first.first;
      const int row = p.first.second;
      
      viewport.setChar(col, row, p.second.type->getRepresentation());
    }
}

BuildControl::BuildControl(ControlStack* stack, World* world, Screen* screen, const BuildOrders::Order& order)
  : Control(stack)
  , world_(world)
  , padRepeater_(2)
  , buildOrders_(world->buildOrders)
  , order_(order)
  , viewport_(screen, world)
  , cursor_(world)
{
}

void BuildControl::render(Screen* screen)
{
  screen->clear();

  const int scrRows = screen->getNumRows() - 4;

  viewport_.setNumRows(scrRows);
  viewport_.renderWorld();

  renderEntities(world_, viewport_);

  if( ticks_ & 4 ) {

    renderBuildOrders(buildOrders_, viewport_);
    
    for( int row = cursor_.getMinWorldRow(); row <= cursor_.getMaxWorldRow(); ++row ) {
      for( int col = cursor_.getMinWorldCol(); col <= cursor_.getMaxWorldCol(); ++col ) {
	viewport_.setChar( col, row,
			   Char('X', order_.type->isFeasible(world_, col, row) ? Font::White : Font::Red, Font::Bold) );
      }
    }
  }

  std::stringstream ss;
  ss << "[" << std::setw(3) << cursor_.getWorldCol() << "/" << std::setw(3) << cursor_.getWorldRow() << "] "
     << ( (mode_ == ModeBuild)
	  ? order_.type->getDescription()
	  : std::string("Cancel") );
  screen->print(0, scrRows, Font::Bold, Font::Blue, ss.str().c_str());
}

void BuildControl::update(vpad::BufferedPad* pad)
{
  ticks_++;

  vpad::BufferedPad::Event event;

  auto pressed = [&](const int button){
    return event.isNull() 
      ? padRepeater_.isPressed(button) 
      : (event.pressed && event.button == button);
  };

  /*
    Y      - Set/Erase
    SELECT - Toggle set/erase
    B      - Cancel
    A      - Submit
   */

  // We need to handle at least one Null-Event
  // so that the repeater gets polled!
  
  do {

    event = pad->nextEvent();
    if( !event.isNull() ) {
      padRepeater_.setPressed(event.button, event.pressed);
    }

    if( event.button == vpad::ButtonY ) {

      if( event.pressed ) {
	cursor_.startDrag();
      }
      else {
	if( cursor_.isDragging() ) {
	  for( int row = cursor_.getMinWorldRow(); row <= cursor_.getMaxWorldRow(); ++row ) {
	    for( int col = cursor_.getMinWorldCol(); col <= cursor_.getMaxWorldCol(); ++col ) {
	      if( mode_ == ModeBuild ) {
		if( order_.type->isFeasible(world_, col, row) ) {
		  buildOrders_.setOrder(col, row, order_);
		}
	      }
	      else {
		buildOrders_.resetOrder(col, row);
	      }
	    }
	  }
	  cursor_.stopDrag();
	}
      }
    }
    else if( event.button == vpad::ButtonSelect ) {
      if( event.pressed ) {
	mode_ = (mode_ == ModeBuild)
	  ? ModeErase
	  : ModeBuild;
      }
    }
    else if( event.button == vpad::ButtonA ) {
      if( event.pressed ) {
	world_->buildOrders.update(buildOrders_);
	pad->flush();
	getControlStack()->pop();
	return;
      }
    }
    else if( event.button == vpad::ButtonB ) {
      if( event.pressed ) {
	pad->flush();
	getControlStack()->pop();
	return;
      }
    }
    else {

      const int d = pad->isPressed(vpad::ButtonTriggerRight)
	? 4
	: 1;

      if( pressed(vpad::ButtonLeft) ) {
	cursor_.addWorldPos(-d, 0);
      }
      if( pressed(vpad::ButtonRight) ) {
	cursor_.addWorldPos( d, 0);
      }
      if( pressed(vpad::ButtonUp) ) {
	cursor_.addWorldPos(0, -d);
      }
      if( pressed(vpad::ButtonDown) ) {
	cursor_.addWorldPos(0,  d);
      }

      viewport_.scrollToWorldPos(cursor_.getWorldCol(), cursor_.getWorldRow());
    }

  } while( !event.isNull() );

  padRepeater_.update(pad);
}

void BuildControl::onEnter()
{
  viewport_.setWorldPos(world_->viewportCol, world_->viewportRow);
  cursor_.setWorldPos(world_->cursorCol, world_->cursorRow);
  viewport_.scrollToWorldPos(world_->cursorCol, world_->cursorRow);
}

void BuildControl::onLeave()
{
  world_->viewportCol = viewport_.getWorldCol();
  world_->viewportRow = viewport_.getWorldRow();
  world_->cursorCol = cursor_.getWorldCol();
  world_->cursorRow = cursor_.getWorldRow();
}
