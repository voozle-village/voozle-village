#ifndef _VIEWPORT_H_
#define _VIEWPORT_H_

class Char;
class Screen;
class World;

class Viewport
{
public:
  Viewport(Screen* screen, World* world);

  void setWorldPos(const int col, const int row);
  void addWorldPos(const int col, const int row);
  
  void setChar(const int wldCol, const int wldRow, const Char& chr);
  void setCharRect(const int wldCol0, const int wldRow0, const int wldCol1, const int wldRow1, const Char& chr);

  void renderWorld();

  void setNumRows(const int numScrRows);

  int getWorldCol() const
  {
    return wldCol_;
  }

  int getWorldRow() const
  {
    return wldRow_;
  }

  void scrollToWorldPos(const int col, const int row);

  bool containsWorldPos(const int col, const int row) const;

private:
  int wldCol_ = 0;
  int wldRow_ = 0;

  Screen* screen_;
  World* world_;
  int numScrRows_ = 0;
};

#endif /* _VIEWPORT_H_ */
