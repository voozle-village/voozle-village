#include "fps.h"
#include "gametime.h"

#include <iomanip>
#include <sstream>

float GameTime::gameTimePerRealTime()
{
  // 1 day in realtime = 10 minutes in game time 
  return (24 * 60) / 10;
}

GameTime GameTime::Frame()
{
  return Frames(1);
}

GameTime GameTime::Frames(const int value)
{
  return GameTime(value * gameTimePerRealTime() / FPS);
}

GameTime GameTime::Seconds(const int value)
{
  return GameTime(value);
}

GameTime GameTime::Minutes(const float value)
{
  return Seconds(static_cast<int>( value * 60.0f ));
}

GameTime GameTime::Hours(const float value)
{
  return Minutes(static_cast<int>( value * 60.0f ));
}

GameTime GameTime::Days(const float value)
{
  return Hours(static_cast<int>( value * 24.0f ));
}

GameTime GameTime::operator+(const GameTime& that) const
{
  return GameTime(seconds_ + that.seconds_);
}

GameTime& GameTime::operator+=(const GameTime& that)
{
  seconds_ += that.seconds_;
  return *this;
}

GameTime GameTime::operator-(const GameTime& that) const
{
  return GameTime(seconds_ - that.seconds_);
}

GameTime& GameTime::operator-=(const GameTime& that)
{
  seconds_ -= that.seconds_;
  return *this;
}

bool GameTime::operator==(const GameTime& that) const
{
  return seconds_ == that.seconds_;
}

bool GameTime::operator!=(const GameTime& that) const
{
  return seconds_ != that.seconds_;
}

bool GameTime::operator>(const GameTime& that) const
{
  return seconds_ > that.seconds_;
}

bool GameTime::operator>=(const GameTime& that) const
{
  return seconds_ >= that.seconds_;
}

bool GameTime::operator<(const GameTime& that) const
{
  return seconds_ < that.seconds_;
}

bool GameTime::operator<=(const GameTime& that) const
{
  return seconds_ <= that.seconds_;
}

int GameTime::getHours() const
{
  return (seconds_ % (24 * 60 * 60)) / (60 * 60);
}

int GameTime::getMinutes() const
{
  return (seconds_ % (60 * 60)) / 60;
}

int GameTime::getSeconds() const
{
  return seconds_ % 60;
}

int GameTime::getValue() const
{
  return seconds_;
}

std::string GameTime::getStringHM() const
{
  std::stringstream ss;
  ss << std::setw(2) << getHours()
     << ":" << std::setw(2) << std::setfill('0') << getMinutes();
  return ss.str();
}

std::string GameTime::getStringHMS() const
{
  std::stringstream ss;
  ss << std::setw(2) << getHours()
     << ":" << std::setw(2) << std::setfill('0') << getMinutes()
     << ":" << std::setw(2) << std::setfill('0') << getSeconds();
  return ss.str();
}

GameTime::GameTime()
  : seconds_(0)
{
}

GameTime::GameTime(const int seconds)
  : seconds_(seconds)
{
}

void GameClock::tick()
{
  gameTime_ += GameTime::Frame();
}

void GameClock::setTime(const GameTime& time)
{
  gameTime_ = time;
}

const GameTime& GameClock::getTime() const
{
  return gameTime_;
}
