#include "entitytypes.h"
#include "tiletypes.h"
#include "world.h"

bool TileType::can_hold_entities() const
{
  return true;
}

void TileType::update(Tile& /*tile*/) const
{
}

std::vector<TileType::MaterialRequirement> TileType::get_material_requirements() const
{
  return std::vector<MaterialRequirement>();
}

template<>
std::string getId(const TileType* tileType)
{
  return tileType->get_id();
}

namespace tiletypes
{
  REGISTER_TILE_TYPE(Null);

  Null::Null()
  {
    c.c = ' ';
    description = "n/a";
    can_be_built_upon = false;
  }

  std::string Null::get_id() const
  {
    return "null";
  }

  bool Null::can_hold_entities() const
  {
    return false;
  }

  REGISTER_TILE_TYPE(DeepWater);

  DeepWater::DeepWater()
  {
    c.c = '~';
    c.color = Font::DarkBlue;
    description = "Deep water";
    can_be_built_upon = false;
  }

  std::string DeepWater::get_id() const
  {
    return "deep_water";
  }

  bool DeepWater::can_hold_entities() const
  {
    return false;
  }

  REGISTER_TILE_TYPE(Water);

  Water::Water()
  {
    c.c = '~';
    c.color = Font::Blue;
    description = "Water";
    can_be_built_upon = false;
  }

  std::string Water::get_id() const
  {
    return "water";
  }

  REGISTER_TILE_TYPE(Dirt);

  Dirt::Dirt()
  {
    c.c = '.';
    description = "Dirt";
  }

  std::string Dirt::get_id() const
  {
    return "dirt";
  }

  std::vector<TileType::MaterialRequirement> Dirt::get_material_requirements() const
  {
    return { { entitytypes::Null::instance(), 0 } };
  }

  REGISTER_TILE_TYPE(Tree);

  Tree::Tree()
  {
    c.c = 'T';
    c.color = Font::Green;
    description = "Tree";
    can_be_built_upon = false;
  }

  std::string Tree::get_id() const
  {
    return "tree";
  }

  bool Tree::can_hold_entities() const
  {
    return false;
  }

  REGISTER_TILE_TYPE(Mountain);

  Mountain::Mountain()
  {
    c.c = '^';
    c.color = Font::DarkGrey;
    description = "Mountain";
    can_be_built_upon = false;
  }

  std::string Mountain::get_id() const
  {
    return "mountain";
  }

  bool Mountain::can_hold_entities() const
  {
    return false;
  }

  REGISTER_TILE_TYPE(HighMountain);

  HighMountain::HighMountain()
  {
    c.c = '^';
    c.color = Font::White;
    description = "High mountain";
    can_be_built_upon = false;
  }

  std::string HighMountain::get_id() const
  {
    return "high_mountain";
  }

  bool HighMountain::can_hold_entities() const
  {
    return false;
  }

  REGISTER_TILE_TYPE(WoodWall);

  WoodWall::WoodWall()
  {
    c.c = '#';
    c.color = Font::Yellow;
    description = "Wood wall";
  }

  std::string WoodWall::get_id() const
  {
    return "wood_wall";
  }

  bool WoodWall::can_hold_entities() const
  {
    return false;
  }

  std::vector<TileType::MaterialRequirement> WoodWall::get_material_requirements() const
  {
    return { { entitytypes::WoodLog::instance(), 2 } };
  }

  REGISTER_TILE_TYPE(StoneWall);

  StoneWall::StoneWall()
  {
    c.c = '#';
    c.color = Font::Grey;
    description = "Stone wall";
  }

  std::string StoneWall::get_id() const
  {
    return "stone_wall";
  }

  bool StoneWall::can_hold_entities() const
  {
    return false;
  }

  std::vector<TileType::MaterialRequirement> StoneWall::get_material_requirements() const
  {
    return { { entitytypes::StoneBlock::instance(), 2 } };
  }

  REGISTER_TILE_TYPE(WoodFloor);

  WoodFloor::WoodFloor()
  {
    c.c = '-';
    c.color = Font::DarkYellow;
    description = "Wood floor";
  }

  std::string WoodFloor::get_id() const
  {
    return "wood_floor";
  }

  std::vector<TileType::MaterialRequirement> WoodFloor::get_material_requirements() const
  {
    return { { entitytypes::WoodLog::instance(), 1 } };
  }

  REGISTER_TILE_TYPE(StoneFloor);

  StoneFloor::StoneFloor()
  {
    c.c = '-';
    c.color = Font::DarkGrey;
    description = "Stone floor";
  }

  std::string StoneFloor::get_id() const
  {
    return "stone_floor";
  }

  std::vector<TileType::MaterialRequirement> StoneFloor::get_material_requirements() const
  {
    return { { entitytypes::StoneBlock::instance(), 1 } };
  }

} // ns tiletypes
