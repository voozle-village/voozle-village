#include "cursor.h"
#include "world.h"

#include <algorithm>

Cursor::Cursor(World* world)
  : world_(world)
{
}

void Cursor::setWorldPos(const int col, const int row)
{
  wldCol1_ = std::max(0, std::min(col, world_->getNumCols() - 1));
  wldRow1_ = std::max(0, std::min(row, world_->getNumRows() - 1));
}

void Cursor::addWorldPos(const int col, const int row)
{
  setWorldPos(wldCol1_ + col, wldRow1_ + row);
}

void Cursor::startDrag()
{
  dragging_ = true;
  wldCol0_ = wldCol1_;
  wldRow0_ = wldRow1_;
}

void Cursor::stopDrag()
{
  dragging_ = false;
}

int Cursor::getMinWorldCol() const
{
  return dragging_
    ? std::min(wldCol0_, wldCol1_)
    : wldCol1_;
}

int Cursor::getMaxWorldCol() const
{
  return dragging_
    ? std::max(wldCol0_, wldCol1_)
    : wldCol1_;
}

int Cursor::getMinWorldRow() const
{
  return dragging_
    ? std::min(wldRow0_, wldRow1_)
    : wldRow1_;
}

int Cursor::getMaxWorldRow() const
{
  return dragging_
    ? std::max(wldRow0_, wldRow1_)
    : wldRow1_;
}
