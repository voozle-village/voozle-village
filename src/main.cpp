#include "controlstack.h"
#include "font.h"
#include "fps.h"
#include "menucontrol.h"
#include "screen.h"
#include "world.h"
#include "worldcontrol.h"

#include <vpad.h>

#include <SDL.h>
#include <SDL_ttf.h>

#include <iostream>
#include <memory>

#include <cstdlib>

void game(SDL_Surface* surface, TTF_Font* ttf_font)
{
  std::cerr << "Frame clock: " << GameTime::Frame().getStringHMS() << std::endl;

  srand(time(nullptr));

  Font font(surface, ttf_font);

  vpad::BufferedPad pad;
  vpad::KeyboardAdapter padAdapter(&pad);

  padAdapter.bindKey( vpad::ButtonLeft, SDLK_LEFT, "" );
  padAdapter.bindKey( vpad::ButtonRight, SDLK_RIGHT, "" );
  padAdapter.bindKey( vpad::ButtonUp, SDLK_UP, "" );
  padAdapter.bindKey( vpad::ButtonDown, SDLK_DOWN, "" );

  padAdapter.bindKey( vpad::ButtonA, SDLK_d, "D" );
  padAdapter.bindKey( vpad::ButtonB, SDLK_s, "S" );
  padAdapter.bindKey( vpad::ButtonX, SDLK_w, "W" );
  padAdapter.bindKey( vpad::ButtonY, SDLK_a, "A" );

  padAdapter.bindKey( vpad::ButtonStart, SDLK_RETURN, "ENTER" );
  padAdapter.bindKey( vpad::ButtonSelect, SDLK_TAB, "TAB" );

  padAdapter.bindKey( vpad::ButtonTriggerLeft, SDLK_q, "Q" );
  padAdapter.bindKey( vpad::ButtonTriggerRight, SDLK_e, "E" );

  Screen screen(surface, font);

  std::unique_ptr<World> world(World::load("world.json.gz"));
  if( !world ) {
    std::cerr << "WARNING: Could not load compressed world file" << std::endl;
    std::unique_ptr<World> plainWorld(World::load("world.json"));
    if( plainWorld ) {
      world.swap(plainWorld);
    }
    else {
      std::cerr << "WARNING: Could not load plain world file" << std::endl;
      std::unique_ptr<World> newWorld(World::generate());
      newWorld->initViewport(&screen);
      world.swap(newWorld);
    }
  }

  ControlStack controlStack;
  controlStack.setPadAdapter(&padAdapter);

  controlStack.push( new MenuControl( &controlStack, 
				      "Main menu",
				      { { "Play", "Start the game", true },
					{ "Quit", "Exit to operating system", true },
					{ "Not available", "Test case for a disabled option.\n\nCannot click.", false } },
				      [&world, &screen](MenuControl* mc, int i)
				      {
					ControlStack* cs = mc->getControlStack();
					if( i == 1 ) {
					  cs->pop();
					}
					else if( i == 0 ) {
					  cs->pop();
					  cs->push(new WorldControl(cs, &*world, &screen));
					}
				      } ) );

  const Uint32 PeriodMS = 1000 / FPS;
  Uint32 ticks0 = SDL_GetTicks();
  Uint32 ticks = 0;

  while(!controlStack.isEmpty()) {
    {
      SDL_Event e;
      while(SDL_PollEvent(&e)) {
	if( e.type == SDL_QUIT ) {
	  controlStack.clear();
	}
	else {
	  padAdapter.onSDLEvent(&e);
	}
      }
    }

    if(!controlStack.isEmpty() ) {
      controlStack.getControl(-1)->render(&screen);
    }

    screen.render(surface, font);
    
    SDL_Flip(surface);

    Uint32 ticks1 = SDL_GetTicks();
    Uint32 dt = ticks1 - ticks0;
    if( dt < PeriodMS ) {
      SDL_Delay(PeriodMS - dt);
      ticks1 = SDL_GetTicks();
      dt = ticks1 - ticks0;
    }
    ticks0 = ticks1;

    ticks += dt;
    while( ticks >= PeriodMS && !controlStack.isEmpty() ) {
      ticks -= PeriodMS;

      controlStack.update(&pad);

      pad.flush();
      screen.update();
    }
  }

  world->save("world.json.gz");
  
  //SDL_Delay(5000);
}


int main(int argc, char** argv)
{
  std::string opt_font = "FreeMono.ttf";
  int opt_font_size = 10;
  int opt_width = 320;
  int opt_height = 240;

  for( int i = 1; i < argc; ++i ) {
    
    std::string arg(argv[i]);
    std::string next_arg( (i + 1 < argc && *argv[i + 1] != '-')
			  ? std::string(argv[i + 1])
			  : std::string() );

    if( arg == "-width" ) {
      if( next_arg.empty() ) {
	std::cerr << "ERROR: Option '" << arg << "' requires argument" << std::endl;
	return 1;
      }
      else {
	opt_width = atoi(next_arg.c_str());
	i++;
      }
    }
    else if( arg == "-height" ) {
      if( next_arg.empty() ) {
	std::cerr << "ERROR: Option '" << arg << "' requires argument" << std::endl;
	return 1;
      }
      else {
	opt_height = atoi(next_arg.c_str());
	i++;
      }
    }
    else if( arg == "-font" ) {
      if( next_arg.empty() ) {
	std::cerr << "ERROR: Option '" << arg << "' requires argument" << std::endl;
	return 1;
      }
      else {
	opt_font = next_arg;
	i++;
      }
    }
    else if( arg == "-fontsize" ) {
      if( next_arg.empty() ) {
	std::cerr << "ERROR: Option '" << arg << "' requires argument" << std::endl;
	return 1;
      }
      else {
	opt_font_size = atoi(next_arg.c_str());
	i++;
      }
    }
    else {
      std::cerr << "ERROR: Unknown option '" << arg << "'" << std::endl;
      return 1;
    }
  }

  if( 0 != SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) ) {
    std::cerr << "ERROR: " << SDL_GetError() << std::endl;
    return 1;
  }

  const int err = [&]() {

    SDL_Surface* screen = SDL_SetVideoMode(opt_width, opt_height, 0, SDL_SWSURFACE);
    if( !screen ) {
      std::cerr << "ERROR: " << SDL_GetError() << std::endl;
      return 1;
    }
    
    SDL_WM_SetCaption("[Voozle Village]", 0);

    return [&]() {
      if( 0 != TTF_Init() ) {
	std::cerr << "ERROR: " << TTF_GetError() << std::endl;
	return 1;
      }
      const int err = [&]() {
	TTF_Font* font = TTF_OpenFont(opt_font.c_str(), opt_font_size);
	if( !font ) {
	  std::cerr << "ERROR: " << TTF_GetError() << std::endl;
	  return 1;
	}
	game(screen, font);
	TTF_CloseFont(font);
	return 0;
      }();
      TTF_Quit();
      return err;
    }();
  }();

  SDL_Quit();

  return err;
}
