#include "char.h"
#include "font.h"

#include <algorithm>
#include <sstream>
#include <iomanip>

namespace 
{
  const SDL_Color colors[Font::MaxColor + 1]{ 
    {255, 255, 255, 0},
    {150, 150, 150, 0},
    { 50,  50,  50, 0},
    {255, 100, 100, 0},
    {150,  40,  40, 0},
    {100, 255, 100, 0},
    { 40, 150,  40, 0},
    {255, 255, 100, 0},
    {150, 150,  40, 0},
    {100, 150, 255, 0},
    { 40, 100, 150, 0},
  };
} // ns 

const SDL_Color* Font::getColor(const Color color)
{
  return &colors[color];
}

Font::Font(SDL_Surface* screen, TTF_Font* font)
  : ascent(TTF_FontAscent(font))
  , height(TTF_FontHeight(font))
{

  const int styles[MaxStyle + 1]{
    TTF_STYLE_NORMAL,
    TTF_STYLE_BOLD,
  };

  for( int style = 0; style < MaxStyle + 1; ++style ) { 
    TTF_SetFontStyle(font, styles[style]);
    for( int color = 0; color < MaxColor + 1; ++color ) {
      for( Uint16 c = 32; c < 127; ++c ) {
	Glyph& glyph = glyphs[style][color][c - 32];
	TTF_GlyphMetrics( font, c, 
			  &glyph.minx, 0,
			  0, &glyph.maxy, 
			  &advance );
	SDL_Surface* tmp = TTF_RenderGlyph_Solid(font, c, colors[color]);
	if( tmp ) {
	  glyph.surface = SDL_ConvertSurface(tmp, screen->format, SDL_SWSURFACE);
	  SDL_FreeSurface(tmp);
	}
      }
    }
  }
}

void Font::print(SDL_Surface* screen, const int x0, const int y0, const Style style, const Color color, const char* text) const
{
  CtrlStack ctrlStack(style, color);

  int y = y0;
  int x = x0;
  for(const char* ptr = text; *ptr; ++ptr) {
    if( *ptr == '\n' ) {
      x = x0;
      y += height;
    }
    else if( isCtrl(ptr) ) {
      ctrlStack.handleCtrl(ptr);
      if( !*ptr )
	break;
    }
    else {
      const Glyph& glyph = glyphs[ctrlStack.getStyle()][ctrlStack.getColor()][*ptr - 32];
      glyph.render(screen, x, y, ascent);
      x += advance;
    }
  }
}

void Font::putc(SDL_Surface* screen, const int x, const int y, const Char& c) const
{
  const Glyph& glyph = glyphs[c.style][c.color][c.c - 32];
  glyph.render(screen, x, y, ascent);
}

CtrlStack::CtrlStack(const Font::Style style, const Font::Color color)
{
  styles_.push_back(style);
  colors_.push_back(color);
}

Font::Style CtrlStack::getStyle() const
{
  return styles_.back();
}

Font::Color CtrlStack::getColor() const
{
  return colors_.back();
}

void CtrlStack::handleCtrl(const char*& ptr)
{
  char buffer[2] { 0, 0 };
  
  if( *ptr == CtrlEnd ) {
    if( !ctrls_.empty() ) {
      if( ctrls_.back() == CtrlStyle ) {
	styles_.pop_back();
      }
      else if( ctrls_.back() == CtrlColor ) {
	colors_.pop_back();
      }
      ctrls_.pop_back();
    }
  }
  else if( *ptr == CtrlStyle ) {
    ctrls_.push_back(CtrlStyle);
    buffer[0] = *(++ptr);
    char* end;
    auto v = strtol(buffer, &end, 16);
    if( end == buffer || v < 0 || v > Font::MaxStyle ) {
      styles_.push_back(styles_.back());
    }
    else {
      styles_.push_back(static_cast<Font::Style>(v));
    }
  }
  else if( *ptr == CtrlColor ) {
    ctrls_.push_back(CtrlColor);
    buffer[0] = *(++ptr);
    char* end;
    auto v = strtol(buffer, &end, 16);
    if( end == buffer || v < 0 || v > Font::MaxColor ) {
      colors_.push_back(colors_.back());
    }
    else {
      colors_.push_back(static_cast<Font::Color>(v));
    }
  }
}

bool isCtrl(const char* ptr)
{
  return 
    *ptr == CtrlStyle ||
    *ptr == CtrlColor ||
    *ptr == CtrlEnd;
}

std::string stripCtrl(std::string s)
{
  bool skip = false;
  auto it = std::remove_if( s.begin(), s.end(),
			    [&skip](const char c) {
			      if( skip ) {
				skip = false;
				return true;
			      }
			      else {
				switch( (CtrlByte) c ) {
				case CtrlStyle:
				case CtrlColor:
				  skip = true;
				  return true;
				case CtrlEnd:
				  return true;
				}
			      }
			      return false;
			    } );
  s.erase(it, s.end());
  return s;
}

std::string ctrl(const Font::Style style)
{
  std::stringstream ss;
  ss << (char) CtrlStyle << std::hex << style;
  return ss.str();
}

std::string ctrl(const Font::Style style, const std::string& text)
{
  return ctrl(style) + text + endCtrl();
}

std::string ctrl(const Font::Color color)
{
  std::stringstream ss;
  ss << (char) CtrlColor << std::hex << color;
  return ss.str();
}

std::string ctrl(const Font::Color color, const std::string& text)
{
  return ctrl(color) + text + endCtrl();
}

std::string endCtrl()
{
  std::stringstream ss;
  ss << (char) CtrlEnd;
  return ss.str();
}
