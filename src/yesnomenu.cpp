#include "controlstack.h"
#include "menucontrol.h"
#include "yesnomenu.h"

void yesNoMenu( ControlStack* cs, 
		const std::string& title,
		const std::string& choice0,
		const std::string& choice1,
		std::function<void()> callback0,
		std::function<void()> callback1 )
{
  cs->push( new MenuControl( cs,
			     title,
			     { { choice0, "", true },
			       { choice1, "", true } },
			     [callback0, callback1](MenuControl* mc, int index) {
			       mc->getControlStack()->pop();
			       if( index == 0 ) {
				 callback0();
			       }
			       else if( index == 1 ) {
				 callback1();
			       }
			     } ) );
}
