#include "screen.h"
#include "viewport.h"
#include "world.h"

#include <algorithm>

Viewport::Viewport(Screen* screen, World* world)
  : screen_(screen)
  , world_(world)
  , numScrRows_(screen->getNumRows())
{
}

void Viewport::setWorldPos(const int col, const int row)
{
  wldCol_ = std::max(0, std::min(col, world_->getNumCols() - screen_->getNumCols()));
  wldRow_ = std::max(0, std::min(row, world_->getNumRows() - numScrRows_));
}

void Viewport::addWorldPos(const int col, const int row)
{
  setWorldPos(wldCol_ + col, wldRow_ + row);
}

void Viewport::setChar(const int wldCol, const int wldRow, const Char& chr)
{
  const int scrCol = wldCol - wldCol_;
  const int scrRow = wldRow - wldRow_;
  if( scrRow < numScrRows_ ) {
    screen_->setChar(scrCol, scrRow, chr);
  }
}

void Viewport::setCharRect(const int wldCol0, const int wldRow0, const int wldCol1, const int wldRow1, const Char& chr)
{
  for( int row = wldRow0; row <= wldRow1; ++row ) {
    for( int col = wldCol0; col <= wldCol1; ++col ) {
      setChar(col, row, chr);
    }
  }
}

void Viewport::renderWorld()
{
  int wldRow = wldRow_;
  for(int scrRow = 0; scrRow < numScrRows_; ++scrRow ) {
    int wldCol = wldCol_;
    for(int scrCol = 0; scrCol < screen_->getNumCols(); ++scrCol ) {
      screen_->setChar(scrCol, scrRow, world_->getTile(wldCol, wldRow).type->c);
      wldCol++;
    }
    wldRow++;
  }
}

void Viewport::setNumRows(const int numScrRows)
{
  numScrRows_ = numScrRows;
  setWorldPos(wldCol_, wldRow_);
}

void Viewport::scrollToWorldPos(const int col, const int row)
{
  if( col < wldCol_ ) {
    setWorldPos(col, wldRow_);
  }
  else if( col >= wldCol_ + screen_->getNumCols() ) {
    setWorldPos(1 + col - screen_->getNumCols(), wldRow_);
  }

  if( row < wldRow_ ){
    setWorldPos(wldCol_, row);
  }
  else if( row >= wldRow_ + numScrRows_ ) {
    setWorldPos(wldCol_, 1 + row - numScrRows_);
  }
}

bool Viewport::containsWorldPos(const int col, const int row) const
{
  return col >= wldCol_ && (col - wldCol_) < screen_->getNumCols()
    && row >= wldRow_ && (row - wldRow_) < numScrRows_;
}
