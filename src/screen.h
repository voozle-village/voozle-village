#ifndef _SCREEN_H_
#define _SCREEN_H_

#include <SDL.h>

#include "font.h"

class Char;

class Screen
{
public:
  Screen(SDL_Surface* screen, const Font& font);
  Screen(const Screen&) = delete;
  ~Screen();
  Screen& operator=(const Screen&) = delete;

  void render(SDL_Surface* screen, const Font& font) const;

  int getNumCols() const { return numCols; }
  int getNumRows() const { return numRows; }

  void setChar(const int x, const int y, const Char& c);
  Char getChar(const int x, const int y) const;

  void print( const int x, const int y, 
	      const Font::Style style, 
	      const Font::Color color,
	      const char* text );

  void printWrapped( const int x, const int y, 
		     const int wrapX,
		     const Font::Style style, 
		     const Font::Color color,
		     const char* text );

  void update();
  void clear();

private:
  const int numCols;
  const int numRows;
  Char* chars = 0;
};

#endif /* _SCREEN_H_ */
