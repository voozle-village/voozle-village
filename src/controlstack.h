#ifndef _CONTROLSTACK_H_
#define _CONTROLSTACK_H_

#include <vpad.h>

#include <vector>

class Control;

class ControlStack
{
public:
  ControlStack();
  ControlStack(const ControlStack&) = delete;
  ~ControlStack();
  ControlStack& operator=(const ControlStack&) = delete;

  void push(Control* control);
  void pop();
  void clear();

  int getSize() const;
  bool isEmpty() const;

  void update(vpad::BufferedPad* pad);

  Control* getControl(const int index);

  void setPadAdapter(vpad::AbstractAdapter* adapter);
  vpad::AbstractAdapter* getPadAdapter();

private:

  struct Action
  {
    enum Type {
      Undefined,
      Pop,
      Push,
    };

    Action(const Type type = Undefined, Control* control = 0);

    Type type;
    Control* control;
  };

  void performActions();

  std::vector<Control*> stack_;
  std::vector<Action> actions_;

  vpad::AbstractAdapter* padAdapter_ = 0;
};

#endif /* _CONTROLSTACK_H_ */
