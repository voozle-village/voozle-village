#include "astar.h"
#include "buildorders.h"
#include "components.h"
#include "voozle.h"
#include "world.h"

#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>

namespace 
{
  struct Path
  {
  public:
    Path(const astar::Path& nodes)
      : nodes_(nodes)
      , index_(1)
    {
      assert(!nodes.empty());
    }

    astar::Node next()
    {
      assert(!isFinished());
      return nodes_[index_++];
    }

    const astar::Node& peek() const
    {
      assert(index_ < nodes_.size());
      return nodes_[index_];
    }

    bool isFinished() const
    {
      return (index_ + 1) >= nodes_.size();
    }

  private:
    astar::Path nodes_;
    size_t index_;
  };
  
  Path* find_path( Entity* ent,
		   const int col0, const int row0,
		   const int col1, const int row1,
		   const bool nextTo )
  {
    World* world = ent->getWorld();
    astar::Path nodes = astar::astar( {col0, row0}, {col1, row1},
				      []( const astar::Node& from, 
					  const astar::Node& to ) {
					return 
					  std::abs( to.first -  from.first)
					+ std::abs(to.second - from.second);
				      },
				      [ent, world, nextTo, col1, row1]( const astar::Node& node ) {
					std::vector<astar::Node> neighbours {
					  {     node.first, node.second - 1 },
					  { node.first + 1, node.second - 1 },
					  { node.first + 1,     node.second },
					  { node.first + 1, node.second + 1 },
					  {     node.first, node.second + 1 },
					  { node.first - 1, node.second + 1 },
					  { node.first - 1,     node.second },
					  { node.first - 1, node.second - 1 }
					};
					auto end = std::remove_if( neighbours.begin(),
								   neighbours.end(),
								   [ent, world, nextTo, col1, row1](const astar::Node& node) {
								     if( nextTo && node.first == col1 && node.second == row1 ) {
								       return false;
								     }
								     else if( node.first < 0 || node.first >= world->getNumCols()
									      || node.second < 0 || node.second >= world->getNumRows() ) {
								       return true;
								     }
								     else {
								       return !world->canMoveTo(ent, node.first, node.second);
								     }
								   } );
					
					neighbours.erase( end, neighbours.end() );
					return neighbours;
				      },
				      []( const astar::Node& /*from*/, const astar::Node& /*to*/ ) {
					//TODO: Map weights
					return 1.0f;
				      } );

    if( nodes.empty() ) {
      return nullptr;
    }
    else {
      return new Path(nodes);
    }
  }

  class Inventory : public Component
  {
  public:
    Inventory(Entity* ent)
      : Component(ent)
    {
    }

    void save(json::Writer& w) override
    {
      w.startArray("entities");
      for( auto e : entities_ ) {
	w.write(e->getId());
      }
      w.endArray();
    }

    class Reader : public json::SkippingObjectReader
    {
    public:
      Reader(Inventory* inv)
	: inv(inv)
      {
      }

      json::ArrayReader* onNamedArray(const std::string& name) override
      {
	typedef decltype(&toInt) Converter;
	typedef std::back_insert_iterator<std::vector<int>> Inserter;
	auto completer = [this]()
	{
	  for( auto id : entityIds ) {
	    if( id < 1 ) {
	      std::cerr << "ERROR: InventoryReader: Invalid id" << std::endl;
	      return false;
	    }
	    else {
	      this->inv->entities_.push_back(nullptr);
	      this->inv->getEntity()->getWorld()->addEntityRef({id, this->inv->entities_, this->inv->entities_.size() - 1});
	    }
	  }
	  return true;
	};
	typedef decltype(completer) Completer;

	if( name == "entities" ) {
	  return new json::ValueArrayReader<Converter, Inserter, Completer>(toInt, Inserter(entityIds), completer);
	}
	else {
	  return json::SkippingObjectReader::onNamedArray(name);
	}
      }

      bool onComplete() override
      {
	return true;
      }

    private:
      Inventory* inv;
      std::vector<int> entityIds;
    };

    json::ObjectReader* createReader() override
    {
      return new Reader(this);
    }
    
    void update() override
    {
    }

    std::string getTypeId() const override
    {
      return "inventory";
    }

    const std::vector<Entity*>& getEntities() const
    {
      return entities_;
    }

    void addEntity(Entity* ent)
    {
      assert(ent);
      entities_.push_back(ent);
    }

    void removeDeletedEntities()
    {
      auto end = std::remove_if( entities_.begin(),
				 entities_.end(),
				 [](const Entity* e) {
				   return e->isDeleted();
				 } );
      entities_.erase( end, entities_.end() );
    }

  private:
    std::vector<Entity*> entities_;
  };

  struct EntityRef
  {
    Entity* container;
    Entity* entity;

    components::MapPosition* getMapPosition() const
    {
      return container
	? container->getComponent<components::MapPosition>()
	: entity->getComponent<components::MapPosition>();
    }
  };

  class MoveTo : public components::Behaviour::Action
  {
    const int col;
    const int row;
    Path* path = nullptr;
    GameTime gameTime = GameTime::Minutes(1);

    void findPath(Entity* ent, const components::MapPosition* pos)
    {
      delete path;
      path = find_path(ent, pos->col, pos->row, col, row, false);
    }

  public:
    MoveTo(const int col, const int row)
      : col(col)
      , row(row)
    {
    }

    ~MoveTo()
    {
      delete path;
    }

    State update(Entity* entity) override
    {
      World* world = entity->getWorld();
      auto pos = entity->getComponent<components::MapPosition>();
     
      gameTime -= GameTime::Frame();
      if( gameTime > GameTime::Seconds(0) ) {
	return InProgress;
      }
      else {

	for(;;) {
	  
	  if( !path ) {
	    findPath(entity, pos);
	    if( !path ) {
	      return Cancelled;
	    }
	    else if( path->isFinished() ) {
	      return Finished;
	    }
	  }

	  {
	    // Try to move one step
	    auto n = path->next();
	    if( world->canMoveTo(entity, n.first, n.second) ) {
	      pos->col = n.first;
	      pos->row = n.second;
	      if( path->isFinished() ) {
		return Finished;
	      }
	      else {
		gameTime = GameTime::Minutes(1);
		return InProgress;
	      }
	    }
	    else {
	      // Cannot move, loop to try new path
	      delete path;
	      path = nullptr;
	    }
	  }
	}
      }
    }

    void cancel(Entity*) override
    {
    }
  };
  
  class MoveNextTo : public components::Behaviour::Action
  {
    const int col;
    const int row;
    Path* path = nullptr;
    GameTime gameTime = GameTime::Minutes(1);

    void findPath(Entity* ent, const components::MapPosition* pos)
    {
      delete path;
      path = find_path(ent, pos->col, pos->row, col, row, true);
    }

  public:
    MoveNextTo(const int col, const int row)
      : col(col)
      , row(row)
    {
    }

    ~MoveNextTo()
    {
      delete path;
    }

    State update(Entity* entity) override
    {
      World* world = entity->getWorld();
      auto pos = entity->getComponent<components::MapPosition>();
     
      gameTime -= GameTime::Frame();
      if( gameTime > GameTime::Seconds(0) ) {
	return InProgress;
      }
      else {

	for(;;) {
	  
	  if( !path ) {
	    findPath(entity, pos);
	    if( !path ) {
	      return Cancelled;
	    }
	    else if( path->isFinished() ) {
	      return Finished;
	    }
	  }

	  // Already there?
	  auto n = path->peek();
	  if( n.first == col && n.second == row ) {
	    return Finished;
	  }
	  else {
	    // Try to move one step
	    auto n = path->next();
	    if( world->canMoveTo(entity, n.first, n.second) ) {
	      pos->col = n.first;
	      pos->row = n.second;
	      gameTime = GameTime::Minutes(1);
	      return InProgress;
	    }
	    else {
	      // Cannot move, loop to try new path
	      delete path;
	      path = nullptr;
	    }
	  }
	}
      }
    }

    void cancel(Entity*) override
    {
    }
  };

  struct PickUp : public components::Behaviour::Action
  {
    EntityRef entityRef;
    GameTime gameTime = GameTime::Minutes(5);

    PickUp(const EntityRef& entityRef)
      : entityRef(entityRef)
    {
    }

    State update(Entity* voozle) override
    {
      gameTime -= GameTime::Frame();
      if( gameTime >= GameTime::Seconds(0) ) {
	return InProgress;
      }

      entityRef.entity->getComponent<components::Reservation>()->entity = nullptr;
      entityRef.entity->getComponent<components::MapRepresentation>()->visible = false;

      voozle->getComponent<Inventory>()->addEntity(entityRef.entity);

      entityRef = { nullptr, nullptr };

      return Finished;
    }

    void cancel(Entity*) override
    {
      if( entityRef.entity ) {
	entityRef.entity->getComponent<components::Reservation>()->entity = nullptr;
      }
    }
  };

  struct BuildAt : public components::Behaviour::Action
  {
    const int col;
    const int row;
    GameTime gameTime = GameTime::Minutes(10);
    bool performed = false;

    BuildAt(const int col, const int row)
      : col(col)
      , row(row)
    {
    }

    State update(Entity* voozle) override
    {
      gameTime -= GameTime::Frame();
      if( gameTime >= GameTime::Seconds(0) ) {
	return InProgress;
      }

      auto inv = voozle->getComponent<Inventory>();

      const auto& order = voozle->getWorld()->buildOrders.getOrder(col, row);
      if( order.reservedFor != voozle ) {
	return Cancelled;
      }
      
      const auto orderType = dynamic_cast<BuildTileOrder*>(order.type.get());
      if( !orderType ) {
	std::cerr << "ERROR: Unexpected build order '" << order.type->getDescription() << "'" << std::endl;
	return Cancelled;
      }

      {
	for( auto& req : orderType->getTileType()->get_material_requirements() ) {
	  for( auto e : inv->getEntities() ) {
	    if( e->getType() == req.first ) {
	      e->setDeleted();
	      req.second--;
	    }
	    if( req.second <= 0 )
	      break;
	  }
	  inv->removeDeletedEntities();
	}
      }
      
      voozle->getWorld()->buildOrders.resetOrder(col, row);
      voozle->getWorld()->setTile(col, row, Tile(orderType->getTileType()));

      performed = true;

      return Finished;
    }

    void cancel(Entity* ent) override
    {
      if (!performed) {
	auto order = ent->getWorld()->buildOrders.getOrder(col, row);
	order.reservedFor = nullptr;
	ent->getWorld()->buildOrders.setOrder(col, row, order);
      }
    }
  };

  bool maybeBuild(Entity* voozle);
  bool maybeWander(Entity* voozle);
  
  class Behaviour : public components::Behaviour
  {
  public:
    Behaviour(Entity* entity)
      : components::Behaviour(entity)
    {
    }

    void think() override
    {
      auto voozle = getEntity();
      World* world = voozle->getWorld();

      // If we are blocking something, move away
      // If we cannot build anything, and we still carry
      // materials, try to put them into storage
      if( maybeBuild(voozle) ) {
	world->log.addDoes(voozle, "goes to build");
      }
      else if( maybeWander(voozle) ) {
	world->log.addDoes(voozle, "goes wandering around");
      }
      else {
	world->log.addDoes(voozle, "does not know what to do!");
      }
    }
  };

  // Find all available materials
  typedef std::map<const EntityType*, std::vector<EntityRef>> MaterialMap;
  MaterialMap find_materials(World* w)
  {
    // TODO: Handle containers
    MaterialMap materials;
    for( auto e : w->getEntities() ) {
      const auto repr = e->getComponent<components::MapRepresentation>();
      if( !repr || !repr->visible ) {
	continue;
      }
      const auto res = e->getComponent<components::Reservation>();
      if( !res || res->entity != nullptr ) {
        continue;
      }
      // FIXME: Filter entities types that cannot be used as materials
      materials[ e->getType() ].push_back( { nullptr, e } );
    }
    return materials;
  }

  // Find build states that can be satisfied given the materials available
  typedef std::vector<BuildOrders::Map::Map::value_type*> CandidateList;
  CandidateList find_candidates(World* w, const MaterialMap& materials) {
    CandidateList candidates;
    for( auto& p : w->buildOrders.getOrders().getMap() ) {
      const auto& order = p.second;
      if( order.reservedFor ) {
	continue;
      }
      const auto orderType = dynamic_cast<BuildTileOrder*>(order.type.get());
      if( !orderType ) {
	std::cerr << "ERROR: Unexpected build order '" << order.type->getDescription() << "'" << std::endl;
	continue;
      }

      bool satisfied = true;
      for( const auto& req : orderType->getTileType()->get_material_requirements() ) {
	const auto type = req.first;
	const auto num = req.second;
	auto it = materials.find(type);
	if( num > 0 && ( it == materials.end() || 
			 (int)(*it).second.size() < num ) ) {
	  satisfied = false;
	  break;
	}
      }
      if( satisfied ) {
	candidates.push_back(&p);
      }
    }
    return candidates;
  }

  bool maybeBuild(Entity* v)
  {
    World* w = v->getWorld();
    MaterialMap materials = find_materials(w);

    // Add materials from inventory
    for( auto e : v->getComponent<Inventory>()->getEntities() ) {
      materials[e->getType()].push_back({v, e});
    }

    CandidateList candidates = find_candidates(w, materials);
    if( candidates.empty() ) {
      return false;
    }

    // Pick closest
    const auto pos = v->getComponent<components::MapPosition>();
    auto it = std::min_element( candidates.begin(),
				candidates.end(),
				[pos]( const CandidateList::value_type a, 
				       const CandidateList::value_type b ) {
				  const int dColA = a->first.first - pos->col;
				  const int dRowA = a->first.second - pos->row;
				  const int dColB = b->first.first - pos->col;
				  const int dRowB = b->first.second - pos->row;
				  return (dColA * dColA + dRowA * dRowA) < (dColB * dColB + dRowB * dRowB);
				} );

    auto lastCol = pos->col;
    auto lastRow = pos->row;

    auto checkPath = [v, &lastCol, &lastRow](const int nextCol, const int nextRow) {
      std::unique_ptr<Path> path( find_path(v, lastCol, lastRow, nextCol, nextRow, true) );
      if(path) {
	lastCol = nextCol;
	lastRow = nextRow;
	return true;
      }
      else {
	return false;
      }
    };

    {
      const auto buildCoords = (*it)->first;
      auto& order = (*it)->second;

      auto beh = v->getComponent<Behaviour>();

      const auto orderType = dynamic_cast<BuildTileOrder*>(order.type.get());
      if( !orderType ) {
	std::cerr << "ERROR: Unexpected build order '" << order.type->getDescription() << "'" << std::endl;
	return false;
      }

      for( const auto& req : orderType->getTileType()->get_material_requirements() ) {
	const auto type = req.first;
	auto num = req.second;
	auto& entities = materials[type];
	std::sort( entities.begin(),
		   entities.end(),
		   [pos](const EntityRef& a, const EntityRef& b) {
		     return a.getMapPosition()->distSquared(*pos) 
		       < b.getMapPosition()->distSquared(*pos);
		   } );

	for( auto& e : entities ) {
	  if( e.container != v ) {
	    // If the material is not in the inventory, go fetch it
	    const auto matPos = e.getMapPosition();
	    
	    if( !checkPath(matPos->col, matPos->row) ) {
	      beh->cancelActions();
	      return false;
	    }
	    
	    e.entity->getComponent<components::Reservation>()->entity = v;
	    beh->addAction( new MoveNextTo(matPos->col, matPos->row) );
	    beh->addAction( new PickUp(e) );
	  }
	  if( --num <= 0 ) {
	    break;
	  }
	}
      }

      if( !checkPath(buildCoords.first, buildCoords.second) ) {
	beh->cancelActions();
	return false;
      }

      beh->addAction(new MoveNextTo(buildCoords.first, buildCoords.second));
      order.reservedFor = v;
      beh->addAction(new BuildAt(buildCoords.first, buildCoords.second));
    }

    return true;
  }

  bool maybeWander(Entity* v)
  {
    const World* world = v->getWorld();
    auto beh = v->getComponent<components::Behaviour>();

    for(int i = 0; i < 100; ++i) {
      const int col = rand() % world->getNumCols();
      const int row = rand() % world->getNumRows();
      if( world->canMoveTo(v, col, row) ) {
	// TODO: Check initial path
	beh->addAction(new MoveTo(col, row));
	return true;
      }
    }
    return false;
  }

} // ns 

namespace entitytypes
{
  REGISTER_ENTITY_TYPE(Voozle);

  std::string Voozle::getId() const 
  {
    return "voozle";
  }
  
  Entity* Voozle::createInstance(World* world, const int id ) const
  {
    Entity* ent = new Entity(this, world, id);

    auto pos = new components::MapPosition(ent);
    pos->blockBits = components::MapPosition::BlockVoozle;
    pos->blockMask = components::MapPosition::BlockSolid;
    ent->addComponent(pos);

    components::MapRepresentation* repr = new components::MapRepresentation(ent);
    repr->chr = Char('@');
    repr->priority = 1;
    ent->addComponent(repr);

    ent->addComponent(new Behaviour(ent));

    ent->addComponent(new Inventory(ent));

    ent->addComponent(new components::Name(ent));
    ent->addComponent(new components::Sex(ent));
    
    return ent;
  }
} // ns entities
