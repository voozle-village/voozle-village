#ifndef _GZPIPE_H_
#define _GZPIPE_H_

#include <string>

#include <cstdio>

namespace gzpipe
{
  class Writer
  {
  public:
    Writer() = default;
    Writer(const Writer&) = delete;
    ~Writer();
    Writer& operator=(const Writer&) = delete;

    FILE* open(const std::string& filename);

  private:
    FILE* fp_ = nullptr;
  };

  class Reader
  {
  public:
    Reader() = default;
    Reader(const Reader&) = delete;
    ~Reader();
    Reader& operator=(const Reader&) = delete;

    FILE* open(const std::string& filename);

  private:
    FILE* fp_ = nullptr;
  };
} // ns gzpipe

#endif /* _GZPIPE_H_ */
