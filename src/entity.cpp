#include "entity.h"
#include "entitytypes.h"
#include "world.h"

#include <iostream>
#include <memory>

Entity::Entity(const EntityType* type, World* world, const int id)
  : type_(type)
  , world_(world)
  , id_( id <= 0 
	 ? world->createNewEntityId()
	 : id )
{
  assert(id_ > 0);
}

Entity::~Entity()
{
  for( auto c : components_ ) {
    delete c;
  }
}

void Entity::save(json::Writer& w)
{
  w.startObject();
  w.write("id", getId());
  w.write("type_id", getType()->getId());
  for( auto c : components_ ) {
    w.startObject(c->getTypeId());
    c->save(w);
    w.endObject();
  }
  w.endObject();
}

Component* Entity::getComponent(const std::string typeId) const
{
  for( auto c : components_ ) {
    if( c->getTypeId() == typeId ) {
      return c;
    }
  }
  return nullptr;
}

void Entity::addComponent(Component* comp)
{
  components_.push_back(comp);
}

void Entity::update()
{
  for( auto c : components_ ) {
    c->update();
  }
}

void Entity::setDeleted()
{
  deleted_ = true;
}

EntityPointerRef::EntityPointerRef(const int id, Entity*& pointer)
  : id(id)
  , pointer(pointer)
{
}

EntityPointerVectorRef::EntityPointerVectorRef(const int id, std::vector<Entity*>& pointers, const size_t index)
  : id(id)
  , pointers(pointers)
  , index(index)
{
}

EntityReader::EntityReader(World* world)
  : world(world)
{
}

bool EntityReader::onNamedValue(const std::string& name, const std::string& value)
{
  if( name == "id" ) {
    bool ok;
    id = toInt(value, ok);
    if( !ok || id < 1 ) {
      std::cerr << "ERROR: EntityReader: Invalid id '" << value << "'" << std::endl;
      return false;
    }
  }
  else if (name == "type_id") {
    if(id < 0 ) {
      std::cerr << "ERROR: EntityReader: type_id before id" << std::endl;
      return false;
    }
    const EntityType* entityType = EntityTypeRegistry::instance().getItem(value);
    if( !entityType ) {
      std::cerr << "ERROR: EntityReader: Could not instantiate entity type '" << value << "'" << std::endl;
      return false;
    }
    std::unique_ptr<Entity> tmpEntity( entityType->createInstance(world, id) );
    if( !tmpEntity ) {
      std::cerr << "ERROR: EntityReader: Could not instantiate entity '" << value << "'" << std::endl;
      return false;
    }
    entity.swap(tmpEntity);
  }
  else {
    return SkippingObjectReader::onNamedValue(name, value);
  }

  return true;
}

json::ArrayReader* EntityReader::onNamedArray(const std::string& name)
{
  std::cerr << "ERROR: EntityReader: Unexpected named array '" << name << "'" << std::endl;
  return nullptr;
}

json::ObjectReader* EntityReader::onNamedObject(const std::string& name)
{
  if( !entity ) {
    std::cerr << "ERROR: EntityReader: Component before entity type_id '" << name << "'" << std::endl;
    return nullptr;
  }
  else {
    auto component = entity->getComponent(name);
    if( !component ) {
      std::cerr << "WARNING: EntityReader: Entity does not have component '" << name << "'" << std::endl;
      return new json::SkippingObjectReader;
    }
    else {
      return component->createReader();
    }
  }
}

bool EntityReader::onComplete()
{
  if( !entity ) {
    std::cerr << "ERROR: EntityReader: Could not read entity" << std::endl;
    return false;
  }
  else {
    world->addEntity(entity.release());
    return true;
  }
}
