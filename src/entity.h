#ifndef _ENTITY_H_
#define _ENTITY_H_

#include "components.h"

#include <jsonreader.h>
#include <jsonwriter.h>

#include <map>
#include <string>
#include <vector>

#include <cassert>

class EntityType;
class World;

class Entity
{
public:
  Entity(const EntityType* type, World* world, const int id);
  Entity(const Entity&) = delete;
  ~Entity();
  Entity& operator=(const Entity&) = delete;

  void save(json::Writer& w);

  const EntityType* getType() const
  {
    return type_;
  }

  World* getWorld()
  {
    return world_;
  }

  int getId() const 
  {
    return id_;
  }

  template<class C>
  C* getComponent() const
  {
    for( auto component : components_ ) {
      C* c = dynamic_cast<C*>(component);
      if( c ) {
	return c;
      }
    }
    return 0;
  }

  Component* getComponent(const std::string typeId) const;

  void addComponent(Component* comp);

  void update();

  void setDeleted();

  bool isDeleted() const
  {
    return deleted_;
  }

private:
  const EntityType* type_;
  World* world_;
  int id_;
  std::vector<Component*> components_;
  bool deleted_ = false;
};

class EntityPointerRef
{
public:
  EntityPointerRef(const int id, Entity*& pointer);

  const int id;
  Entity*& pointer;
};

class EntityPointerVectorRef
{
public:
  EntityPointerVectorRef(const int id, std::vector<Entity*>& pointers, const size_t index);

  const int id;
  std::vector<Entity*>& pointers;
  const size_t index;
};

class EntityReader : public json::SkippingObjectReader
{
public:
  EntityReader(World* world);
  bool onNamedValue(const std::string& name, const std::string& value) override;
  json::ArrayReader* onNamedArray(const std::string& name) override;
  json::ObjectReader* onNamedObject(const std::string& name) override;
  bool onComplete() override;
private:
  World* world;
  int id = 0;
  std::unique_ptr<Entity> entity;
};

#endif /* _ENTITY_H_ */
