#ifndef _NAMES_H_
#define _NAMES_H_

#include "components.h"

#include <unordered_set>
#include <string>

class Entity;

class NameGenerator
{
public:
  std::string generateName(const components::Sex::Value sex);
  void generateName(Entity* ent);
  void addName(const std::string& name);
  void resetNames();

private:
  std::unordered_set<std::string> names_;
};

#endif /* _NAMES_H_ */
