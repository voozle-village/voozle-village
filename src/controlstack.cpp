#include "control.h"
#include "controlstack.h"

#include <cassert>
#include <iostream>

ControlStack::ControlStack()
{
}

ControlStack::~ControlStack()
{
  if( !isEmpty() ) {
    std::cerr << "WARNING: Control stack is not empty!" << std::endl;
    clear();
  }
}

void ControlStack::push(Control* control)
{
  actions_.push_back(Action(Action::Push, control));
}

void ControlStack::pop()
{
  actions_.push_back(Action(Action::Pop));
}

void ControlStack::clear()
{
  performActions();
  while( !stack_.empty() ) {
    stack_.back()->onLeave();
    delete stack_.back();
    stack_.pop_back();
  }
}

int ControlStack::getSize() const
{
  int size = (int) stack_.size();
  for( auto& action : actions_ ) {
    if( action.type == Action::Pop ) {
      size--;
    }
    else if( action.type == Action::Push ) {
      size++;
    }
  }
  return size;
}

bool ControlStack::isEmpty() const
{
  return getSize() == 0;
}

Control* ControlStack::getControl(const int index)
{
  performActions();

  if( index < 0 ) {
    return getControl(getSize() + index);
  }
  else if( index < getSize() ) {
    return stack_[(unsigned) index];
  }
  else {
    return 0;
  }
}

void ControlStack::setPadAdapter(vpad::AbstractAdapter* adapter)
{
  padAdapter_ = adapter;
}

vpad::AbstractAdapter* ControlStack::getPadAdapter()
{
  return padAdapter_;
}

void ControlStack::update(vpad::BufferedPad* pad)
{
  performActions();

  if( !stack_.empty() ) {
    getControl(-1)->update(pad);
  }

  performActions();
}

void ControlStack::performActions()
{  
  for( auto action : actions_ ) {
    switch(action.type) {
    case Action::Undefined:
      break;
    case Action::Pop:
      {
	assert(!stack_.empty());
	stack_.back()->onLeave();
	delete stack_.back();
	stack_.pop_back();
	if( !stack_.empty() ) {
	  stack_.back()->onEnter();
	}
      }
      break;

     case Action::Push:
      {
	if( !stack_.empty() ) {
	  stack_.back()->onLeave();
	}
	stack_.push_back(action.control);
	stack_.back()->onEnter();
      }
      break;
    }
  }
  
  actions_.clear();
}

ControlStack::Action::Action(const Type type, Control* control)
  : type(type)
  , control(control)
{
}
