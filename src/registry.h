#ifndef _REGISTRY_H_
#define _REGISTRY_H_

#include <map>
#include <string>
#include <vector>

template<class T>
std::string getId(const T* item);

template<class T>
class Registry
{
public:
  typedef T ItemType;
  typedef std::map<std::string, ItemType*> Map;

  template<class S>
  class Registrator
  {
  public:
    typedef S ItemType;
    typedef Registry<Registry::ItemType> RegistryType;

    Registrator()
    {
      RegistryType::instance().registerItem(ItemType::instance());
    }

    Registrator(const Registrator&) = delete;
    
    ~Registrator()
    {
      RegistryType::instance().unregisterItem(ItemType::instance());
    }

    Registrator& operator=(const Registrator&) = delete;
  };

  static Registry& instance()
  {
    static Registry registry;
    return registry;
  }

  Registry(const Registry&) = delete;
  Registry& operator=(const Registry&) = delete;

  void registerItem(ItemType* item)
  {
    items_[ getId(item) ] = item;
  }

  void unregisterItem(ItemType* item)
  {
    items_.erase( getId(item) );
  }

  ItemType* getItem(const std::string& id) const
  {
    auto it = items_.find(id);
    if( it == items_.end() ) {
      return 0;
    }
    else {
      return (*it).second;
    }
  }

  std::vector<std::string> getItemIds() const
  {
    std::vector<std::string> ids;
    for( auto& it : items_ ) {
      ids.push_back(it.first);
    }
    return ids;
  }

  const Map& getItemMap() const
  {
    return items_;
  }

private:
  Registry() {}
  Map items_;
};

#endif /* _REGISTRY_H_ */
