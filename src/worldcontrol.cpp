#include "buildcontrol.h"
#include "controlstack.h"
#include "font.h"
#include "menucontrol.h"
#include "screen.h"
#include "world.h"
#include "worldcontrol.h"
#include "yesnomenu.h"

#include <vpad.h>

#include <iomanip>
#include <sstream>

void renderEntities(World* world, Viewport& viewport)
{
  {
    SparseMap<Entity*> entities;
    for( auto e : world->getEntities() ) {
      const components::MapRepresentation* repr = e->getComponent<components::MapRepresentation>();
      if( repr && repr->visible ) {
	const components::MapPosition* pos = e->getComponent<components::MapPosition>();
	if( pos ) {
	  if( viewport.containsWorldPos(pos->col, pos->row) ) {
	    auto existing = entities.getItem({pos->col, pos->row}, nullptr);
	    if( existing ) {
	      if( repr->priority > existing->getComponent<components::MapRepresentation>()->priority ) {
		entities.setItem({pos->col, pos->row}, e);
	      }
	    }
	    else {
	      entities.setItem({pos->col, pos->row}, e);
	    }
	  }
	}
      }
    }

    for( auto p : entities.getMap() ) {
      viewport.setChar( p.first.first, p.first.second, 
			p.second->getComponent<components::MapRepresentation>()->chr );
    }
  }
}

void WorldControl::render(Screen* screen)
{
  screen->clear();

  const int numScrRow = screen->getNumRows() - 4; 
  
  viewport_.setNumRows(numScrRow);
  viewport_.renderWorld();

  renderEntities(world_, viewport_);

  if( ticks_ & 4 ) {

    renderBuildOrders( world_->buildOrders, viewport_ );

    viewport_.setChar( cursor_.getWorldCol(), cursor_.getWorldRow(), Char('X', Font::White, Font::Bold) );
  }

  const Tile& cursor_tile = world_->getTile(cursor_.getWorldCol(), cursor_.getWorldRow());
  
  {
    std::stringstream ss;
    ss << "[" << std::setw(3) << cursor_.getWorldCol() << "/" << std::setw(3) << cursor_.getWorldRow() << "] " << cursor_tile.type->description;
    screen->print(0, numScrRow, Font::Bold, Font::Blue, ss.str().c_str());
  }

  {
    screen->print( screen->getNumCols() - 7, numScrRow,
		   Font::Bold, Font::Blue, (std::string("[") + world_->clock.getTime().getStringHM() + std::string("]")).c_str() );
  }
  
  {
    int row = numScrRow + 1;
    for( int i = (int) world_->log.getSize() - 1; i >= 0; --i ) {
      const std::string line = world_->log.getLine(i);
      if( !line.empty() ) {
	screen->print( 2, row++, Font::Normal, Font::Grey, line.c_str() );
      }
    }
  }

}

void WorldControl::update(vpad::BufferedPad* pad)
{
  world_->update();

  ticks_++;

  vpad::BufferedPad::Event event;

  auto pressed = [&](const int button){
    return event.isNull() 
      ? padRepeater_.isPressed(button) 
      : (event.pressed && event.button == button);
  };

  // We need to handle at least one Null-Event
  // so that the repeater gets polled!
  
  do {

    event = pad->nextEvent();
    if( !event.isNull() ) {
      padRepeater_.setPressed(event.button, event.pressed);
    }

    const int d = pad->isPressed(vpad::ButtonTriggerRight)
      ? 4
      : 1;

    if( pad->isPressed(vpad::ButtonY) ) {

      const int oldScrollCol = viewport_.getWorldCol();
      const int oldScrollRow = viewport_.getWorldRow();

      if( pressed(vpad::ButtonLeft) ) {
	viewport_.addWorldPos(-d, 0);
      }
      if( pressed(vpad::ButtonRight) ) {
	viewport_.addWorldPos( d, 0);
      }
      if( pressed(vpad::ButtonUp) ) {
	viewport_.addWorldPos(0, -d);
      }
      if( pressed(vpad::ButtonDown) ) {
	viewport_.addWorldPos(0,  d);
      }
      
      cursor_.addWorldPos( viewport_.getWorldCol() - oldScrollCol, 
			   viewport_.getWorldRow() - oldScrollRow );
    }
    else {

      if( pressed(vpad::ButtonLeft) ) {
	cursor_.addWorldPos(-d, 0);
      }
      else if( pressed(vpad::ButtonRight) ) {
	cursor_.addWorldPos( d, 0);
      }
      else if( pressed(vpad::ButtonUp) ) {
	cursor_.addWorldPos(0, -d);
      }
      else if( pressed(vpad::ButtonDown) ) {
	cursor_.addWorldPos(0,  d);
      }
      else if( pressed(vpad::ButtonStart) ) {
	pad->flush();
	gameMenu();
	return;
      }

      viewport_.scrollToWorldPos(cursor_.getWorldCol(), cursor_.getWorldRow());
    }

  } while( !event.isNull() );

  padRepeater_.update(pad);
}

void WorldControl::onEnter()
{
  viewport_.setWorldPos(world_->viewportCol, world_->viewportRow);
  cursor_.setWorldPos(world_->cursorCol, world_->cursorRow);
  viewport_.scrollToWorldPos(world_->cursorCol, world_->cursorRow);
}

void WorldControl::onLeave()
{
  world_->viewportCol = viewport_.getWorldCol();
  world_->viewportRow = viewport_.getWorldRow();
  world_->cursorCol = cursor_.getWorldCol();
  world_->cursorRow = cursor_.getWorldRow();
}

void WorldControl::gameMenu()
{
  std::vector<MenuOption> options {
    { "Build", "", true },
    { "Quit", "Quit game", true }
  };

  getControlStack()->push( new MenuControl( getControlStack(),
					    "Game menu",
					    options,
					    [this](MenuControl* mc, int index) {
					      if( index == 0 ) {
						buildMenu();
					      }
					      else if( index == 1 ) {
						ControlStack* cs = mc->getControlStack();
						yesNoMenu( cs,
							   "Really Quit?",
							   "Quit",
							   "Cancel",
							   [cs]() {
							     cs->clear();
							   } );
					      }
					    },
					    MenuControl::cancelMenu ) );
}

void WorldControl::buildMenu()
{
  std::vector<std::string> ids;
  for( const auto& p : TileTypeRegistry::instance().getItemMap() ) {
    const std::vector<TileType::MaterialRequirement>& mr = p.second->get_material_requirements();
    if( !mr.empty() ) {
      ids.push_back(p.first);
    }
  }

  std::vector<MenuOption> options;
  for( auto& id : ids ) {
    const TileType* type = TileTypeRegistry::instance().getItem(id);
    options.push_back( { type->description, "", true } );
  }
  
  getControlStack()->push( new MenuControl( getControlStack(),
					    "Build",
					    options,
					    [this, ids](MenuControl* mc, int index) {
					      ControlStack* cs = mc->getControlStack();
					      const int n = cs->getSize() - 1;
					      for( int i = 0; i < n; ++i ) {
						cs->pop();
					      }
					      const std::string& id = ids[index];
					      cs->push( new BuildControl(cs, world_, screen_, BuildOrders::Order(new BuildTileOrder(TileTypeRegistry::instance().getItem(id)))) );
					    },
					    MenuControl::cancelMenu ) );
}
