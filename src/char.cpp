#include "char.h"

Char::Char(const int c, const Font::Color color, const Font::Style style)
  : c(c)
  , color(color)
  , style(style)
{
}
