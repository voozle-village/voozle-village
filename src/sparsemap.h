#ifndef _SPARSEMAP_H_
#define _SPARSEMAP_H_

#include <map>

template<class T>
class SparseMap
{
public:
  typedef T ItemType;
  typedef std::pair<int, int> Coord;
  typedef std::map<Coord, ItemType> Map;

  void setItem(const Coord& coord, const ItemType item)
  {
    data_[coord] = item;
  }

  void resetItem(const Coord& coord)
  {
    data_.erase(coord);
  }

  const ItemType& getItem(const Coord& coord, const ItemType& fallback) const
  {
    const auto& it = data_.find(coord);
    if( it != data_.end() ) {
      return (*it).second;
    }
    return fallback;
  }

  ItemType& getItem(const Coord& coord, ItemType& fallback)
  {
    const auto& it = data_.find(coord);
    if( it != data_.end() ) {
      return (*it).second;
    }
    return fallback;
  }
  
  const Map& getMap() const
  {
    return data_;
  }

  Map& getMap()
  {
    return data_;
  }

private:
  Map data_;
};

template<class T>
class BoundedSparseMap
{
public:
  typedef T ItemType;
  typedef std::pair<int, int> Coord;
  typedef std::map<Coord, ItemType> Map;

  BoundedSparseMap()
    : numCols_(0)
    , numRows_(0)
  {
  }

  BoundedSparseMap(const int numCols, const int numRows)
    : numCols_(numCols)
    , numRows_(numRows)
  {
  }

  void setItem(const Coord& coord, const ItemType item)
  {
    if( containsCoord(coord) ) {
      data_[coord] = item;
    }
  }

  void resetItem(const Coord& coord)
  {
    if( containsCoord(coord) ) {
      data_.erase(coord);
    }
  }

  ItemType getItem(const Coord& coord, const ItemType fallback) const
  {
    if( containsCoord(coord) ) {
      const auto& it = data_.find(coord);
      if( it != data_.end() ) {
	return (*it).second;
      }
    }
    return fallback;
  }
  
  const Map& getMap() const
  {
    return data_;
  }

private:
  bool containsCoord(const Coord& coord) const
  {
    return coord.first >= 0
      && coord.first < numCols_
      && coord.second >= 0
      && coord.second <= numRows_;
  }
  
  int numCols_;
  int numRows_;
  Map data_;
};

#endif /* _SPARSEMAP_H_ */
