#ifndef _CONTROL_H_
#define _CONTROL_H_

#include <vpad.h>

class ControlStack;
class Screen;

class Control
{
public:
  Control(ControlStack* stack);

  virtual ~Control() {}
  virtual void render(Screen* screen) = 0;
  virtual void update(vpad::BufferedPad* pad) = 0;
  virtual void onEnter() {}
  virtual void onLeave() {}

  ControlStack* getControlStack() { return controlStack_; }

private:
  ControlStack* controlStack_;
};

#endif /* _CONTROL_H_ */
