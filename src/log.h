#ifndef _LOG_H_
#define _LOG_H_

#include <deque>
#include <string>

class Entity;

class Log
{
public:
  size_t getSize() const;
  std::string getLine(const size_t index) const;
  void addLine(const std::string& line);
  void addSays(Entity* ent, const std::string& line);
  void addDoes(Entity* ent, const std::string& line);

private:
  std::deque<std::string> lines_;
};

#endif /* _LOG_H_ */
