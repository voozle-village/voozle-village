#ifndef _STRINGUTILS_H_
#define _STRINGUTILS_H_

#include <string>

std::string toUpper(std::string s);
bool endsWith(const std::string& s, const std::string& end);

int toInt(const std::string& s, bool& ok);
float toFloat(const std::string& s, bool& ok);
bool toBool(const std::string& s, bool& ok);

#endif /* _STRINGUTILS_H_ */
