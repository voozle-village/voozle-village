#ifndef _ENTITYTYPEPROPERTIES_H_
#define _ENTITYTYPEPROPERTIES_H_

#include "entitytypes.h"

#include <vector>

namespace entitytypeproperties
{
  class BuildRequirements : public EntityType::Property
  {
  public:
    struct EntityRequirement
    {
      const EntityType* entityType = nullptr;
      int amount = 0;
    };

    BuildRequirements(EntityType* entityType);

    std::vector<EntityRequirement> entityRequirements;
  };

} // ns entitytypeproperties

#endif /* _ENTITYTYPEPROPERTIES_H_ */
