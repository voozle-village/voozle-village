#ifndef _TILETYPES_H_
#define _TILETYPES_H_

#include "char.h"
#include "registry.h"
#include "singleton.h"

#include <map>
#include <string>

class EntityType;
struct Tile;

class TileType
{
public:
  typedef std::pair<EntityType*, int> MaterialRequirement;

  TileType() = default;
  TileType(const TileType&) = delete;
  virtual ~TileType() {}
  TileType& operator=(const TileType&) = delete;

  virtual std::string get_id() const = 0; 
  virtual bool can_hold_entities() const;

  virtual std::vector<MaterialRequirement> get_material_requirements() const;
  virtual void update(Tile& tile) const;
  
  Char c;
  std::string description;
  bool can_be_built_upon = true;
};

template<>
std::string getId(const TileType* tileType);

class TileTypeRegistry : public Registry<TileType> {};
#define REGISTER_TILE_TYPE( cls ) TileTypeRegistry::Registrator<cls> _register##cls

template<class Child>
struct TileTypeBase : public SingletonBase<Child, TileType> {};

namespace tiletypes
{
  struct Null : public TileTypeBase<Null>
  {
    Null();
    std::string get_id() const override;
    bool can_hold_entities() const override;
  };

  struct DeepWater : public TileTypeBase<DeepWater>
  {
    DeepWater();
    std::string get_id() const override;
    bool can_hold_entities() const override;
  };

  struct Water : public TileTypeBase<Water>
  {
    Water();
    std::string get_id() const override;
  };

  struct Dirt : public TileTypeBase<Dirt>
  {
    Dirt();
    std::string get_id() const override;
    std::vector<MaterialRequirement> get_material_requirements() const override;
  };

  struct Tree : public TileTypeBase<Tree>
  {
    Tree();
    std::string get_id() const override;
    bool can_hold_entities() const override;
  };

  struct Mountain : public TileTypeBase<Mountain>
  {
    Mountain();
    std::string get_id() const override;
    bool can_hold_entities() const override;
  };

  struct HighMountain : public TileTypeBase<HighMountain>
  {
    HighMountain();
    std::string get_id() const;
    bool can_hold_entities() const override;
  };

  struct WoodWall : public TileTypeBase<WoodWall>
  {
    WoodWall();
    std::string get_id() const override;
    bool can_hold_entities() const override;
    std::vector<MaterialRequirement> get_material_requirements() const override;
  };

  struct StoneWall : public TileTypeBase<StoneWall>
  {
    StoneWall();
    std::string get_id() const override;
    bool can_hold_entities() const override;
    std::vector<MaterialRequirement> get_material_requirements() const override;
  };

  struct WoodFloor : public TileTypeBase<WoodFloor>
  {
    WoodFloor();
    std::string get_id() const override;
    std::vector<MaterialRequirement> get_material_requirements() const override;
  };

  struct StoneFloor : public TileTypeBase<StoneFloor>
  {
    StoneFloor();
    std::string get_id() const override;
    std::vector<MaterialRequirement> get_material_requirements() const override;
  };
  
} // ns tiletypes

#endif /* _TILETYPES_H_ */
