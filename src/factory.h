#ifndef _FACTORY_H_
#define _FACTORY_H_

#include <functional>
#include <map>
#include <string>

template<class T>
class Factory
{
public:
  typedef T ItemType;
  typedef std::function<ItemType*()> Constructor;

  template<class S>
  class Registrator
  {
    typedef S ItemType;

    static ItemType* create()
    {
      return new ItemType;
    }

  public:
    Registrator(const std::string& id)
    {
      Factory::instance().registerConstructor(id, create);
    }
  };

  static Factory& instance()
  {
    static Factory factory;
    return factory;
  }

  Factory(const Factory&) = delete;
  Factory& operator=(const Factory&) = delete;

  ItemType* createInstance(const std::string& id)
  {
    auto it = constructors_.find(id);
    if( it == constructors_.end() ) {
      return nullptr;
    }
    else {
      return (*it).second();
    }
  }

  void registerConstructor(const std::string& id, Constructor constructor)
  {
    constructors_[id] = constructor;
  }

private:
  Factory() = default;
  std::map<std::string, Constructor> constructors_;
};

#endif /* _FACTORY_H_ */
