#include "char.h"
#include "font.h"
#include "screen.h"

#include <iostream>

Screen::Screen(SDL_Surface* screen, const Font& font)
  : numCols(screen->w / font.getAdvance())
  , numRows(screen->h / font.getHeight())
{
  std::cerr << "SCREEN: " << numCols << "x" << numRows << std::endl;
  chars = new Char[ numCols * numRows ];
}

Screen::~Screen()
{
  delete [] chars;
}

void Screen::update()
{
}

void Screen::render(SDL_Surface* screen, const Font& font) const
{
  SDL_FillRect(screen, 0, SDL_MapRGBA(screen->format, 0, 0, 0, 255));

  int i = 0;
  int y = 0;
  for(int row = 0; row < numRows; ++row ) {
    int x = 0;
    for(int col = 0; col < numCols; ++col ) {
      const Char& c = chars[i++];
      if( c.c ) {
	font.putc(screen, x, y, c);
      }
      x += font.getAdvance();
    }
    y += font.getHeight();
  }
}

void Screen::setChar(const int x, const int y, const Char& c)
{
  if( x >= 0 && x < numCols && y >= 0 && y < numRows ) {
    chars[x + y * numCols] = c;
  }
}

Char Screen::getChar(const int x, const int y) const
{
  if( x >= 0 && x < numCols && y >= 0 && y < numRows ) {
    return chars[x + y * numCols];
  }
  else {
    return Char();
  }
}

void Screen::print( const int x0, const int y0, 
		    const Font::Style style, 
		    const Font::Color color,
		    const char* text )
{
  printWrapped(x0, y0, 0, style, color, text);
}

void Screen::printWrapped( const int x0, const int y0, 
			   const int wrapX,
			   const Font::Style style, 
			   const Font::Color color,
			   const char* text )
{
  CtrlStack ctrlStack(style, color);

  int x = x0;
  int y = y0;
  for( const char* ptr = text; *ptr; ++ptr ) {
    
    if( x0 < wrapX && x >= wrapX ) {
      x = x0;
      y++;
      while( *ptr == ' ' ) ++ptr;
      if( *ptr == '\n' ) {
	continue;
      }
    }

    if( *ptr == '\n' ) {
      x = x0;
      y++;
    }
    else if( isCtrl(ptr) ) {
      ctrlStack.handleCtrl(ptr);
      if( !*ptr ) {
	break;
      }
    }
    else {
      setChar(x, y, Char{(int) *ptr, ctrlStack.getColor(), ctrlStack.getStyle()});
      x++;
    }
  }
}

void Screen::clear()
{
  for( int i = 0; i < numRows * numCols; ++i ) {
    chars[i] = Char();
  }
}
