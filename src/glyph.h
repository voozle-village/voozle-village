#ifndef _GLYPH_H_
#define _GLYPH_H_

#include <SDL.h>

class Glyph
{
public:
  int minx;
  int maxy;
  SDL_Surface* surface;
  
  Glyph();
  Glyph(const Glyph&) = delete;
  ~Glyph();
  Glyph& operator=(const Glyph&) = delete;

  void render(SDL_Surface* screen, const int x, const int y, const int ascent) const;

private:
};

#endif /* _GLYPH_H_ */
