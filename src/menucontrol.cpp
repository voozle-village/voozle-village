#include "controlstack.h"
#include "menucontrol.h"
#include "screen.h"

void MenuControl::cancelMenu(MenuControl* mc, int)
{
  mc->getControlStack()->pop();
}

MenuControl::MenuControl( ControlStack* stack,
			  const std::string& title,
			  const std::vector<MenuOption>& options, 
			  Callback callback,
			  Callback cancel )
  : Control(stack)
  , title_(title)
  , options_(options)
  , callback_(callback)
  , cancel_(cancel)
  , max_title_length_(0)
{
  for( auto& o : options ) {
    max_title_length_ = std::max(max_title_length_, (int) o.title.size() );
  }
}
  
void MenuControl::render(Screen* screen)
{
  screen->clear();
  screen->print( 0, 0, Font::Bold, Font::Blue, ("[" + title_ + "]").c_str() );

  int scr_row = 2;
  for( int i = scroll_row_; i < (int) options_.size() && scr_row < screen->getNumRows() - 1; ++i ) {
    screen->print( 2,
		   scr_row++,
		   (i == cursor_row_) ? Font::Bold : Font::Normal,
		   (i == cursor_row_)
		   ? ( options_[i].enabled
		       ? Font::Yellow
		       : Font::Red )
		   : ( options_[i].enabled
		       ? Font::White
		       : Font::Grey ),
		   options_[i].title.c_str() );
  }

  { 
    int scr_row = 2;
    int scr_col = std::max( screen->getNumCols() / 2, max_title_length_ + 4 );
    screen->printWrapped( scr_col, scr_row, screen->getNumCols() - 2, 
			  Font::Normal, Font::White,
			  options_[cursor_row_].description.c_str() );
  }
}

void MenuControl::update(vpad::BufferedPad* pad)
{
  vpad::BufferedPad::Event event;
  while( !(event = pad->nextEvent()).isNull() ) {
    if( event.pressed ) {
      switch(event.button) {
      case vpad::ButtonUp:
	if( cursor_row_ > 0 ) {
	  cursor_row_--;
	}
	break;
      case vpad::ButtonDown:
	if( cursor_row_ + 1 < (int) options_.size() ) {
	  cursor_row_++;
	}
	break;
      case vpad::ButtonLeft:
	cursor_row_ = std::max(0, cursor_row_ - 8);
	break;
      case vpad::ButtonRight:
	cursor_row_ = std::min((int)(options_.size() - 1), cursor_row_ + 8);
	break;
      case vpad::ButtonY:
	if( options_[cursor_row_].enabled ) {
	  callback_(this, cursor_row_);
	}
	pad->flush();
	return;
      case vpad::ButtonB:
	cancel_(this, cursor_row_);
	pad->flush();
	return;
      }
    }
  }
}

void MenuControl::onEnter()
{
}
