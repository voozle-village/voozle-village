#ifndef _COMPONENT_H_
#define _COMPONENT_H_

#include "char.h"

#include <jsonreader.h>
#include <jsonwriter.h>

#include <string>
#include <vector>

class Entity;

class Component
{
public:
  Component(Entity* entity);
  Component(const Component&) = delete;
  virtual ~Component();
  Component& operator=(const Component&) = delete;

  Entity* getEntity()
  {
    return entity_;
  }

  virtual void save(json::Writer& w);
  virtual json::ObjectReader* createReader();

  virtual void update();

  virtual std::string getTypeId() const = 0;

private:
  Entity* entity_;
};

namespace components
{
  struct MapPosition : public Component
  {
    enum BlockBits
    {
      BlockVoozle    = 0x0001,
      BlockSolid     = 0x0002,
      BlockContainer = 0x0004,
      BlockItems     = 0x0008,
    };

    MapPosition(Entity* entity);

    int distSquared(const MapPosition& that) const;

    void save(json::Writer& w) override;
    json::ObjectReader* createReader() override;

    std::string getTypeId() const override;

    int col = 0;
    int row = 0;
    unsigned blockBits = 0; // Blocking bits that the entity has
    unsigned blockMask = 0; // Blocking bits that the entity conflicts with
  };

  struct MapRepresentation : public Component
  {
    MapRepresentation(Entity* entity);

    void save(json::Writer& w) override;
    json::ObjectReader* createReader() override;

    std::string getTypeId() const override;

    bool visible = true;
    int priority = 0;
    Char chr;
  };

  struct Reservation : public Component
  {
    Reservation(Entity* ent);

    void save(json::Writer& w) override;
    json::ObjectReader* createReader() override;

    std::string getTypeId() const override;
    
    Entity* entity = nullptr;
  };

  class Behaviour : public Component
  {
  public:
    struct Action
    {
      Action() = default;
      Action(const Action&) = delete;
      virtual ~Action() {}
      Action& operator=(const Action&) = delete;

      enum State {
	InProgress,
	Finished,
	Cancelled,
      };

      virtual State update(Entity* ent) = 0;
      virtual void cancel(Entity* ent) = 0;
    };

    Behaviour(Entity* entity);
    ~Behaviour();

    void update() override;
    std::string getTypeId() const override;

    void addAction(Action* action);
    void cancelActions();

    virtual void think() = 0;

  protected:
    Action* currentAction() const;
    Action* nextAction();

  private:
    void deleteActions();

    std::vector<Action*> actions_;
    size_t index_ = 0;
  };

  struct Name : public Component
  {
    Name(Entity* entity);

    void save(json::Writer& w) override;
    json::ObjectReader* createReader() override;

    std::string getTypeId() const override;

    std::string name = "n/a";
  };

  struct Sex : public Component
  {
    enum Value {
      Male,
      Female
    };

    Sex(Entity* entity);

    void save(json::Writer& w) override;
    json::ObjectReader* createReader() override;

    std::string getTypeId() const override;

    Value value = Male;
  };

} // ns components

#endif /* _COMPONENT_H_ */
