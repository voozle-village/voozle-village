#include "gzpipe.h"
#include "names.h"
#include "screen.h"
#include "stringutils.h"
#include "tiletypes.h"
#include "voozle.h"
#include "world.h"

#include <jsonreader.h>

#include <algorithm>
#include <functional>
#include <iostream>
#include <map>
#include <memory>

#include <cassert>
#include <cstdlib>
#include <cstdio>

static int ceilDiv(const int a, const int b)
{
  return (a % b)
    ? ( (a / b) + 1  )
    : (a / b);
}

TileUpdateWindow::TileUpdateWindow(World* world)
  : world_(world)
  , tick_(GameTime::Frames(ceilDiv(world->getNumCols(), NumCols) * ceilDiv(world->getNumRows(), NumRows)))
{
  std::cerr << "Tile clock: " << tick_.getStringHMS() << std::endl;
}

void TileUpdateWindow::update()
{
  const int nextCol = std::min(col_ + NumCols, world_->getNumCols());
  const int nextRow = std::min(row_ + NumRows, world_->getNumRows());

  for( int row = row_; row < nextRow; ++row ) {
    for( int col = col_; col < nextCol; ++col ) {
      Tile& t = world_->tiles_[col + row * world_->getNumCols()];
      t.time += tick_;
      t.type->update(t);
    }
  }

  col_ = nextCol;
  if( col_ >= world_->getNumCols() ) {
    col_ = 0;
    row_ = nextRow;
    if( row_ >= world_->getNumRows() ) {
      row_ = 0;
    }
  }
}

// TODO: Move this to fileutils or something
namespace
{
  class FileReader
  {
  public:
    FileReader() = default;
    FileReader(const FileReader&) = delete;
    ~FileReader()
    {
      if( fp_ ) {
	fclose(fp_);
      }
    }
    FileReader& operator=(const FileReader&) = delete;

    FILE* open(const std::string& filename)
    {
      if( fp_ ) {
	fclose(fp_);
	fp_ = nullptr;
      }

      fp_ = fopen(filename.c_str(), "rb");
      return fp_;
    }

  private:
    FILE* fp_ = nullptr;
  };

  class FileWriter
  {
  public:
    FileWriter() = default;
    FileWriter(const FileWriter&) = delete;
    ~FileWriter()
    {
      if( fp_ ) {
	fclose(fp_);
      }
    }
    FileWriter& operator=(const FileWriter&) = delete;

    FILE* open(const std::string& filename)
    {
      if( fp_ ) {
	fclose(fp_);
	fp_ = nullptr;
      }

      fp_ = fopen(filename.c_str(), "wb+");
      return fp_;
    }

  private:
    FILE* fp_ = nullptr;
  };

} // ns

class WorldReader : public json::SkippingObjectReader
{
  int numCols = 0;
  int numRows = 0;
  int viewportCol = 0;
  int viewportRow = 0;
  int cursorCol = 0;
  int cursorRow = 0;

public:
  bool onNamedValue(const std::string& name, const std::string& value) override
  {
    bool ok;
    if( name == "world_version" ) {
    }
    else if( name == "num_cols" ) {
      numCols = toInt(value, ok);
      if( !ok || numCols < 1 ) {
	std::cerr << "ERROR: WorldReader: Invalid num_cols = '" << value << "'" << std::endl;
	return false;
      }
    }
    else if( name == "num_rows" ) {
      numRows = toInt(value, ok);
      if( !ok || numRows < 1 ) {
	std::cerr << "ERROR: WorldReader: Invalid num_rows = '" << value << "'" << std::endl;
	return false;
      }
    }
    else if( name == "viewport_col" ) {
      viewportCol = toInt(value, ok);
      if( !ok || viewportCol < 0 || viewportCol >= numCols ) {
	std::cerr << "WARNING: WorldReader: Invalid viewport_col = '" << value << "'" << std::endl;
	viewportCol = 0;
      }
    }
    else if( name == "viewport_row" ) {
      viewportRow = toInt(value, ok);
      if( !ok || viewportRow < 0 || viewportRow >= numRows ) {
	std::cerr << "WARNING: WorldReader: Invalid viewport_row = '" << value << "'" << std::endl;
	viewportRow = 0;
      }
    }
    else if( name == "cursor_col" ) {
      cursorCol = toInt(value, ok);
      if( !ok || cursorCol < 0 || cursorCol >= numCols ) {
	std::cerr << "WARNING: WorldReader: Invalid cursor_col = '" << value << "'" << std::endl;
	cursorCol = viewportCol;
      }
    }
    else if( name == "cursor_row" ) {
      cursorRow = toInt(value, ok);
      if( !ok || cursorRow < 0 || cursorRow >= numRows ) {
	std::cerr << "WARNING: WorldReader: Invalid cursor_row = '" << value << "'" << std::endl;
	cursorRow = viewportRow;
      }
    }
    else {
      return SkippingObjectReader::onNamedValue(name, value);
    }
    
    return true;
  }

  json::ArrayReader* onNamedArray(const std::string& name) override
  {
    if( name == "tiles" ) {
      if( numCols < 1 || numRows < 1 ) {
	std::cerr << "ERROR: Tile array before world dimensions" << std::endl;
	return nullptr;
      }
      else {
	std::unique_ptr<World> tmp(new World(numCols, numRows));
	world.swap(tmp);
      }

      return new json::ObjectArrayReader( [this](const size_t index) {
	  return new TileReader(this->world.get(), index % (size_t) this->numCols, index / (size_t) this->numCols);
	},
	[this](const size_t index) {
	  return index == (size_t)(this->numCols * this->numRows);
	} );
    }
    else if( name == "build_orders" ) {
      if( world->maxEntityId_ != 0) {
	std::cerr << "ERROR: WorldReader: Entities already loaded" << std::endl;
      }
      return new json::ObjectArrayReader( [this](const size_t /*index*/) {
	  return new BuildOrderReader(this->world.get());
	},
	[](const size_t /*index*/) {
	  return true;
	} );
    }
    else if( name == "entities" ) {
      if( world->maxEntityId_ != 0) {
	std::cerr << "ERROR: WorldReader: Entities already loaded" << std::endl;
      }
      return new json::ObjectArrayReader( [this](const size_t /*index*/) {
	  return new EntityReader(this->world.get());
	},
	[](const size_t /*index*/) {
	  return true;
	} );
    }
    else {
      return SkippingObjectReader::onNamedArray(name);
    }
  }

  bool onComplete() override
  {
    const bool ok = world->resolveEntityIdRefs();
    if( !ok ) {
      std::cerr << "ERROR: WorldReader: Could not resolve entities" << std::endl;
    }
    return ok;
  }

  std::unique_ptr<World> world;
};

World* World::load(const std::string& filename)
{
  std::shared_ptr<json::Reader> reader(new WorldReader);

  {
    std::unique_ptr<gzpipe::Reader> gzReader;
    std::unique_ptr<FileReader> plainReader;
    
    FILE* fp = nullptr;
    if( endsWith(filename, ".gz") ) {
      auto tmp = std::unique_ptr<gzpipe::Reader>(new gzpipe::Reader);
      gzReader.swap(tmp);
      fp = gzReader->open(filename);
    }
    else {
      auto tmp = std::unique_ptr<FileReader>(new FileReader);
      plainReader.swap(tmp);
      fp = plainReader->open(filename);
    }
    if( !fp ) {
      std::cerr << "ERROR: Could not open file '" << filename << "' for reading" << std::endl;
      return nullptr;
    }

    if( !json::parse(fp, reader) ) {
      return nullptr;
    }
    else {
      return static_cast<WorldReader*>(reader.get())->world.release();
    }
  }
}

static const double Depth = 100;

// Returns y >= x with y = 2^n
static int next_power_of_two(const int x)
{
  int y = 1;
  while( x > y ) {
    y *= 2;
  }
  return y;
}

static double drand()
{
  return 1.0 - 2.0 * static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
}

static double reduce(const double x)
{
  return x * 0.8;
}

static void filter( const int w, const int h,
		    std::function<double(const int, const int)> get,
		    std::function<void(const int, const int, const double)> set )
{
  for( int j = 0; j < h; ++j ) {
    for( int i = 0; i < w; ++i ) {

      const int max_jj = std::min(h - 1, j + 1);
      const int max_ii = std::min(w - 1, i + 1);

      double a = 0;
      int n = 0;
      for( int jj = std::max(0, j - 1); jj <= max_jj; ++jj ) {
	for( int ii = std::max(0, i - 1); ii <= max_ii; ++ii ) {
	  a += get(ii, jj);
	  n++;
	}
      }

      set(i, j, a / static_cast<double>(n));
    }
  }
}

static void displace( const int x0, const int y0,
		      const int x1, const int y1, 
		      const double range,
		      std::function<double(const int, const int)> get,
		      std::function<void(const int, const int, const double)> set )
{
  if( x0 + 1 >= x1 || y0 + 1 >= y1 ) {
    return;
  }

  const int mid_x = x0 + (x1 - x0) / 2;
  const int mid_y = y0 + (y1 - y0) / 2;
    
  set(mid_x,    y0, (get(x0, y0) + get(x1, y0)) / 2.0);
  set(   x1, mid_y, (get(x1, y0) + get(x1, y1)) / 2.0);
  set(mid_x,    y1, (get(x0, y1) + get(x1, y1)) / 2.0);
  set(   x0, mid_y, (get(x0, y0) + get(x0, y1)) / 2.0);
  set(mid_x, mid_y, (get(x0, y0) + get(x1, y0) + get(x0, y1) + get(x1, y1)) / 4.0 + drand() * range);

  const int next_range = reduce(range);

  displace( x0, y0,
	    mid_x, mid_y,
	    next_range, get, set );

  displace( mid_x, y0,
	    x1, mid_y,
	    next_range, get, set );

  displace( x0, mid_y,
	    mid_x, y1,
	    next_range, get, set );

  displace( mid_x, mid_y,
	    x1, y1,
	    next_range, get, set );
}

namespace 
{
void gen_map(World* world)
{
  const int map_s = next_power_of_two(std::max(world->getNumCols(), world->getNumRows())) + 1;
  double* map = new double[ map_s * map_s ];

  auto set = [&](const int x, const int y, const double val) {
    map[x + y * map_s] = val;
  };

  auto get = [&](const int x, const int y) {
    return map[x + y * map_s];
  };

  set(        0,         0, drand() * Depth);
  set(map_s - 1,         0, drand() * Depth);
  set(        0, map_s - 1, drand() * Depth);
  set(map_s - 1, map_s - 1, drand() * Depth);

  displace( 0, 0, map_s - 1, map_s - 1, reduce(Depth), get, set );

  filter(map_s, map_s, get, set);
  filter(map_s, map_s, get, set);

  std::map<double, std::pair<int, TileType*>> tile_mapping;
  for( int row = 0; row < world->getNumRows(); ++row ) {
    for( int col = 0; col < world->getNumCols(); ++col ) {
      
      const double h = get(col, row);
      auto it = tile_mapping.find(h);
      if( it == tile_mapping.end() ){
	tile_mapping[h] = std::pair<int, TileType*>(1, tiletypes::Null::instance());
      }
      else {
	((*it).second).first++;
      }
    }
  }

  auto get_percent = [&world](const double percent) {
      const double n = world->getNumCols() * world->getNumRows();
      return percent * n * 0.01;
  };
  
  std::vector<std::pair<TileType*, double>> tile_dist {
    {    tiletypes::DeepWater::instance(), get_percent(10) },
    {        tiletypes::Water::instance(), get_percent(10) },
    {         tiletypes::Dirt::instance(), get_percent(40) },
    {         tiletypes::Tree::instance(), get_percent(25) },
    {     tiletypes::Mountain::instance(), get_percent(10) },
    { tiletypes::HighMountain::instance(), get_percent( 5) }
  };

  int i = 0;
  for( auto& p : tile_mapping ) {
    if( tile_dist[i].second <= 0.0 && i + 1 < (int) tile_dist.size() ) {
	i++;
    }
    p.second.second = tile_dist[i].first;
    tile_dist[i].second -= p.second.first;
  }
  
  for( int row = 0; row < world->getNumRows(); ++row ) {
    for( int col = 0; col < world->getNumCols(); ++col ) {
      
      const double h = get(col, row);

      Tile tile;
      tile.type = tile_mapping[h].second;
      world->setTile(col, row, tile);
    }
  }
  
  delete [] map;
}
} // ns 

static void gen_dirt_map(World* w)
{
  for( int row = 0; row < w->getNumRows(); ++row ) {
    for( int col = 0; col < w->getNumCols(); ++col ) {
      const Tile t(tiletypes::Dirt::instance());
      w->setTile(col, row, t);
    }
  }
}

static void gen_voozles(World* w)
{
  for(int i = 0; i < 5; ++i) {
    Entity* v = entitytypes::Voozle::instance()->createInstance(w, 0);
    v->getComponent<components::Sex>()->value = static_cast<components::Sex::Value>(rand() % 2);
    w->nameGenerator.generateName(v);
    auto pos = v->getComponent<components::MapPosition>();
    for(;;) {
      const int col = rand() % w->getNumCols();
      const int row = rand() % w->getNumRows();
      if( w->canMoveTo( v, col, row ) ) {
	pos->col = col;
	pos->row = row;
	w->addEntity(v);
	break;
      }
    }
  }
}

static void gen_materials(World* w)
{
  for(int i = 0; i < 100; ++i) {
    Entity* e = (rand() & 1)
      ? entitytypes::WoodLog::instance()->createInstance(w, 0)
      : entitytypes::StoneBlock::instance()->createInstance(w, 0);

    components::MapPosition* pos = e->getComponent<components::MapPosition>();
    for(;;) {
      const int col = rand() % w->getNumCols();
      const int row = rand() % w->getNumRows();
      // TODO: Semantics of "move" are not quite correct...
      if( w->canMoveTo( e, col, row ) ) {
	pos->col = col;
	pos->row = row;
	w->addEntity(e);
	break;
      }
    }
  }
}

World* World::generate()
{ 
  World* world = new World();
  gen_dirt_map(world);
  gen_voozles(world);
  gen_materials(world);
  return world;
}

World::World(const int numCols, const int numRows)
  : numCols_(numCols)
  , numRows_(numRows)
  , tiles_(new Tile[numCols * numRows])
  , tileUpdateWindow_(this)
{
}

World::~World()
{
  delete [] tiles_;
  
  for( auto e : entities_ ) {
    delete e;
  }
}

const Tile& World::getTile(const int col, const int row) const
{
  static const Tile null{};
  if( col < 0 || col >= getNumCols() || row < 0 || row >= getNumRows() ) {
    return null;
  }
  else {
    return tiles_[ col + row * getNumCols() ];
  }
}

void World::setTile(const int col, const int row, const Tile& tile)
{
  assert(tile.type && (tile.type != tiletypes::Null::instance()));

  if( col < 0 || col >= getNumCols() || row < 0 || row >= getNumRows() ) {
    return;
  }
  else {
    tiles_[ col + row * getNumCols() ] = tile;
  }
}

void World::save(json::Writer& w)
{
  w.startObject();
  
  w.write("world_version", FormatVersion);
  w.write("num_cols", getNumCols());
  w.write("num_rows", getNumRows());
  w.write("viewport_col", viewportCol);
  w.write("viewport_row", viewportRow);
  w.write("cursor_col", cursorCol);
  w.write("cursor_row", cursorRow);
  
  w.startArray("tiles");
  for( int i = 0; i < getNumCols() * getNumRows(); ++i ) {
    assert( tiles_[i].type && (tiles_[i].type != tiletypes::Null::instance()) );
    tiles_[i].save(w);
  }
  w.endArray();
  
  w.startArray("build_orders");
  for( const auto& p : buildOrders.getOrders().getMap() ) {
    w.startObject();
    w.write("col", p.first.first);
    w.write("row", p.first.second);
    w.write("order_type_id", p.second.type->getId());
    p.second.type->save(w);
    w.endObject();
  }
  w.endArray();

  w.startArray("entities");
  for( auto e : entities_ ) {
    e->save(w);
  }
  w.endArray();

  w.endObject();
}

bool World::save(const std::string& filename)
{
  std::unique_ptr<gzpipe::Writer> gzWriter;
  std::unique_ptr<FileWriter> plainWriter;
  
  FILE* fp = nullptr;
  if( endsWith(filename, ".gz") ) {
    auto tmp = std::unique_ptr<gzpipe::Writer>(new gzpipe::Writer);
    gzWriter.swap(tmp);
    fp = gzWriter->open(filename);
  }
  else {
    auto tmp = std::unique_ptr<FileWriter>(new FileWriter);
    plainWriter.swap(tmp);
    fp = plainWriter->open(filename);
  }
  if( !fp ) {
    std::cerr << "ERROR: Could not open file '" << filename << "' for writing" << std::endl;
    return nullptr;
  }
  
  json::Writer writer(fp);
  save(writer);
  return true;
}

void World::update()
{
  clock.tick();

  tileUpdateWindow_.update();

  for( auto e : entities_ ) {
    e->update();
  }

  auto end = std::remove_if( entities_.begin(),
			     entities_.end(),
			     [](const Entity* e) {
			       if( e->isDeleted() ) {
				 delete e;
				 return true;
			       }
			       else {
				 return false;
			       }
			     } );
  
  entities_.erase(end, entities_.end());
}

std::vector<Entity*> World::getEntitiesAt(const int col, const int row) const
{
  std::vector<Entity*> result;
  for( auto e : entities_ ) {
    auto repr = e->getComponent<components::MapRepresentation>();
    if( repr &&  repr->visible ) {
      auto pos = e->getComponent<components::MapPosition>();
      if( pos && pos->col == col && pos->row == row ) {
	result.push_back(e);
      }
    }
  }
  return result;
}

Entity* World::getTopEntityAt(const int col, const int row) const
{
  auto entities = getEntitiesAt(col, row);
  if( entities.empty() ) {
    return nullptr;
  }
  else {
    auto it = std::max_element( entities.begin(),
				entities.end(),
				[](const Entity* a, const Entity* b) {
				  return a->getComponent<components::MapRepresentation>()->priority
				    < b->getComponent<components::MapRepresentation>()->priority;
				} );
    return *it;
  }
}

void World::addEntity(Entity* ent)
{
  const int id = ent->getId();
  assert(id > 0);
  assert(entitiesById_.find(id) == entitiesById_.end());
  maxEntityId_ = std::max(maxEntityId_, id);
  entities_.push_back(ent);
  entitiesById_[id] = ent;
}

void World::addEntityRef(const EntityPointerRef& ref)
{
  entityPointerRefs_.push_back(ref);
}

void World::addEntityRef(const EntityPointerVectorRef& ref)
{
  entityPointerVectorRefs_.push_back(ref);
}

bool World::canMoveTo(const Entity* ent, const int col, const int row) const
{
  auto pos = ent->getComponent<components::MapPosition>();

  const unsigned blockBits = pos->blockBits;
  unsigned blockMask = pos->blockMask;

  Tile tile = getTile(col, row);
  if( !tile.type || !tile.type->can_hold_entities() ) {
    return false;
  }
  else {
    const auto entities = getEntitiesAt(col, row);
    for( auto e : entities ) {
      auto repr = e->getComponent<components::MapRepresentation>();
      if( !repr || !repr->visible ) {
	continue;
      }
      else {
	auto pos = e->getComponent<components::MapPosition>();
	if( pos ) {
	  blockMask |= pos->blockMask;
	  if( (blockMask & blockBits) ||
	      (blockMask & pos->blockBits ) ) {
	    return false;
	  }
	}
      }
    }
  }
  return true;
}

void World::initViewport(Screen* screen)
{
  viewportCol = (getNumCols() - screen->getNumCols()) / 2;
  viewportRow = (getNumRows() - screen->getNumRows()) / 2;
  cursorCol = getNumCols() / 2;
  cursorRow = getNumRows() / 2;
}

int World::createNewEntityId()
{
  return ++maxEntityId_;
}

bool World::resolveEntityIdRefs()
{
  for( auto& ref : entityPointerRefs_ ) {
    auto it = entitiesById_.find(ref.id);
    if( it == entitiesById_.end() || !(*it).second ) {
      std::cerr << "ERROR: Could not resolve entity id " << ref.id << std::endl;
      return false;
    }
    ref.pointer = (*it).second;
  }
  entityPointerRefs_.clear();

  for( auto& ref : entityPointerVectorRefs_ ) {
    auto it = entitiesById_.find(ref.id);
    if( it == entitiesById_.end() || !(*it).second ) {
      std::cerr << "ERROR: Could not resolve entity id " << ref.id << std::endl;
      return false;
    }
    ref.pointers[ref.index] = (*it).second;
  }
  entityPointerVectorRefs_.clear();

  return true;
}
