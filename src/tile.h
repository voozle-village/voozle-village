#ifndef _TILE_H_
#define _TILE_H_

#include "gametime.h"
#include "tiletypes.h"

#include <jsonreader.h>
#include <jsonwriter.h>

class World;

struct Tile
{
  Tile() = default;
  Tile(const TileType* type, const GameTime& time = GameTime()); 

  void save(json::Writer& w);

  const TileType* type = tiletypes::Null::instance();
  GameTime time;
};

class TileReader : public json::ObjectReader
{
public:
  TileReader(World* world, const int col, const int row);
  bool onNamedValue(const std::string& name, const std::string& value) override;
  json::ArrayReader* onNamedArray(const std::string& name) override;
  json::ObjectReader* onNamedObject(const std::string& name) override;
  bool onComplete() override;
private:
  World* world;
  const int col;
  const int row;
  Tile tile;
};

#endif /* _TILE_H_ */
