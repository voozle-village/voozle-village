#ifndef _VOOZLE_H_
#define _VOOZLE_H_

#include "entitytypes.h"

namespace entitytypes
{
  struct Voozle : public EntityTypeBase<Voozle>
  {
    std::string getId() const override;
    Entity* createInstance(World* world, const int id) const override;
  };
} // ns entitytypes

#endif /* _VOOZLE_H_ */
