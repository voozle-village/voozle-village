#ifndef _CHAR_H_
#define _CHAR_H_

#include "font.h"

class Char
{
public:
  Char(const int c = 0, const Font::Color color = Font::White, const Font::Style style = Font::Normal);

  int c = 0;
  Font::Color color = Font::White;
  Font::Style style = Font::Normal;
};

#endif /* _CHAR_H_ */
