#ifndef _YESNOMENU_H_
#define _YESNOMENU_H_

class ControlStack;

#include <functional>
#include <string>

void yesNoMenu( ControlStack* cs, 
		const std::string& title,
		const std::string& choice0,
		const std::string& choice1,
		std::function<void()> callback0,
		std::function<void()> callback1 = [](){} );

#endif /* _YESNOMENU_H_ */
