#include "stringutils.h"

#include <algorithm>

#include <cstdlib>

std::string toUpper(std::string s)
{
  std::transform(s.begin(), s.end(),s.begin(), ::toupper);
  return s;
} 

bool endsWith(const std::string& s, const std::string& end)
{
  return s.rfind(end) == s.size() - end.size();
}

int toInt(const std::string& s, bool& ok)
{
  char* end;
  const int val = strtol(s.c_str(), &end, 10);
  ok = !s.empty() && *end == '\0';
  return ok ? val : 0;
}

float toFloat(const std::string& s, bool& ok)
{
  char* end;
  const float val = strtof(s.c_str(), &end);
  ok = !s.empty() && *end == '\0';
  return ok ? val : 0;
}

bool toBool(const std::string& s, bool& ok)
{
  ok = !s.empty();
  const std::string us(toUpper(s));
  return us == "1" || us == "TRUE" || us == "YES";
}
