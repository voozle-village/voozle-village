#ifndef _CURSOR_H_
#define _CURSOR_H_

class World;

class Cursor
{
public:
  Cursor(World* world);

  void setWorldPos(const int col, const int row);
  void addWorldPos(const int col, const int row);

  void startDrag();
  void stopDrag();

  int getWorldCol() const 
  {
    return wldCol1_;
  }

  int getWorldRow() const 
  {
    return wldRow1_;
  }

  int getMinWorldCol() const;
  int getMaxWorldCol() const;
  int getMinWorldRow() const;
  int getMaxWorldRow() const;

  bool isDragging() const
  {
    return dragging_;
  }

private:
  World* world_;
  int wldCol0_ = 0;
  int wldRow0_ = 0;
  int wldCol1_ = 0;
  int wldRow1_ = 0;
  bool dragging_ = false;
};

#endif /* _CURSOR_H_ */
