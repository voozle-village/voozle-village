#include "components.h"
#include "entity.h"
#include "entitytypes.h"

EntityType::Property::Property(EntityType* entityType)
{
  entityType->properties_.push_back(this);
}

EntityType::Property::~Property()
{
}

EntityType::~EntityType()
{
  for( auto p : properties_ ) {
    delete p;
  }
}

template<>
std::string getId(const EntityType* entityType)
{
  return entityType->getId();
}

namespace entitytypes
{
  REGISTER_ENTITY_TYPE(Null);

  std::string Null::getId() const
  {
    return "null";
  }

  Entity* Null::createInstance(World* /*world*/, const int /*id*/) const
  {
    return nullptr;
  }

  REGISTER_ENTITY_TYPE(WoodLog);

  std::string WoodLog::getId() const
  {
    return "wood_log";
  }

  Entity* WoodLog::createInstance(World* world, const int id) const
  {
    Entity* ent = new Entity(this, world, id);
    ent->addComponent(new components::MapPosition(ent));
    auto repr = new components::MapRepresentation(ent);
    repr->chr = Char('/');
    ent->addComponent(repr);
    ent->addComponent(new components::Reservation(ent));
    return ent;
  }

  REGISTER_ENTITY_TYPE(StoneBlock);

  std::string StoneBlock::getId() const
  {
    return "stone_block";
  }

  Entity* StoneBlock::createInstance(World* world, const int id) const
  {
    Entity* ent = new Entity(this, world, id);

    ent->addComponent(new components::MapPosition(ent));

    {
      auto repr = new components::MapRepresentation(ent);
      repr->chr = Char('*');
      ent->addComponent(repr);
    }

    ent->addComponent(new components::Reservation(ent));

    return ent;
  }

} // ns
