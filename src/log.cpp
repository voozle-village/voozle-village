#include "components.h"
#include "entity.h"
#include "log.h"
#include "stringutils.h"

#include <sstream>

size_t Log::getSize() const
{
  return lines_.size();
}

std::string Log::getLine(const size_t index) const
{
  if( index < getSize() ) {
    return lines_[index];
  }
  else {
    return std::string();
  }
}

void Log::addLine(const std::string& line)
{
  lines_.push_back(line);
  while( getSize() > 50 ) {
    lines_.pop_front();
  }
}

static std::string formatName(const std::string& name)
{
  return ctrl(Font::Bold, ctrl(Font::Yellow, toUpper(name)));
}

void Log::addSays(Entity* ent, const std::string& line)
{
  auto name = ent->getComponent<components::Name>();
  if( name ) {
    addLine( formatName(name->name) + ": " + line);
  }
  else {
    std::stringstream ss;
    ss << "ENT " << ent->getId() << ": " << line;
    addLine(ss.str());
  }
}

void Log::addDoes(Entity* ent, const std::string& line)
{
  auto name = ent->getComponent<components::Name>();
  if( name ) {
    addLine( formatName(name->name) + " " + line);
  }
  else {
    std::stringstream ss;
    ss << "ENT " << ent->getId() << " " << line;
    addLine(ss.str());
  }
}
