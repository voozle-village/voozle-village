#ifndef _ASTAR_H_
#define _ASTAR_H_

#include <functional>
#include <map>
#include <set>
#include <vector>

namespace astar
{
  typedef std::pair<int, int> Node;
  typedef std::vector<Node> Path;

  typedef std::function<float(const Node&, const Node&)> ValueFunction;
  typedef std::function<std::vector<Node>(const Node&)> NeighbourFunction;

  Path astar( const Node& start,
	      const Node& goal,
	      ValueFunction heuristicFunction,
	      NeighbourFunction neighbourFunction,
	      ValueFunction distanceFunction );

} // ns astar

#endif /* _ASTAR_H_ */
