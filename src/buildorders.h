#ifndef _BUILDORDERS_H_
#define _BUILDORDERS_H_

#include "char.h"
#include "factory.h"
#include "sparsemap.h"

#include <jsonreader.h>
#include <jsonwriter.h>

#include <memory>

class Entity;
class TileType;
class World;

class OrderType
{
public:
  virtual ~OrderType() {}
  virtual std::string getId() const = 0;
  virtual std::string getDescription() const = 0;
  virtual Char getRepresentation() const = 0;
  virtual bool isFeasible(const World* world, const int col, const int row) const = 0;
  virtual void save(json::Writer& w) = 0;
  virtual json::ObjectReader* createReader() = 0;
};

typedef Factory<OrderType> OrderTypeFactory;

#define REGISTER_ORDER_TYPE(cls) OrderTypeFactory::Registrator<cls> _register##cls(cls::Id())

class BuildOrders
{
public:
  struct Order
  {
    Order(OrderType* type = nullptr)
      : type(type)
    {
    }
    
    std::shared_ptr<OrderType> type;
    Entity* reservedFor = nullptr;
  };

  typedef SparseMap<Order> Map;

  void setOrder(const int col, const int row, const Order& order)
  {
    orders_.setItem(Map::Coord(col, row), order);
  }

  void resetOrder(const int col, const int row)
  {
    orders_.resetItem(Map::Coord(col, row));
  }

  const Order& getOrder(const int col, const int row) const
  {
    return orders_.getItem(Map::Coord(col, row), Order());
  }

  const Map& getOrders() const
  {
    return orders_;
  }

  Map& getOrders()
  {
    return orders_;
  }

  void update(const BuildOrders& that);

private:
  Map orders_;
};

class BuildOrderReader : public json::ObjectReader
{
public:
  BuildOrderReader(World* world);
  bool onNamedValue(const std::string& name, const std::string& value) override;
  json::ArrayReader* onNamedArray(const std::string& name) override;
  json::ObjectReader* onNamedObject(const std::string& name) override;
  bool onComplete() override;

private:
  World* world;
  BuildOrders::Order order;
  int col = -1;
  int row = -1;
  std::unique_ptr<json::ObjectReader> typeReader;
};

class BuildTileOrder : public OrderType
{
public:
  static std::string Id() { return "build_tile"; }

  BuildTileOrder(const TileType* tileType = nullptr);

  std::string getId() const override;
  std::string getDescription() const override;
  Char getRepresentation() const override;
  bool isFeasible(const World* world, const int col, const int row) const override;
  void save(json::Writer& w) override;
  json::ObjectReader* createReader() override;

  const TileType* getTileType() const
  {
    return tileType_;
  }

  void setTileType(const TileType* tileType)
  {
    tileType_ = tileType;
  }

private:
  const TileType* tileType_;
};

#endif /* _BUILDORDERS_H_ */
