#include "entity.h"
#include "names.h"

#include <iostream>
#include <vector>

#include <cstdlib>

std::string NameGenerator::generateName(const components::Sex::Value sex)
{
  std::vector<std::string> firstSillables {
    "Ja",
    "Je",
    "Ji",
    "Jo",
    "Ju",
    "Ma",
    "Me",
    "Mi",
    "Mo",
    "Mu",
    "La",
    "Le",
    "Li",
    "Lo",
    "Lu",
    "Ka",
    "Ke",
    "Ki",
    "Ko",
    "Ku",
  };

  std::vector<std::string> midSillables {
    "na",
    "ne",
    "ni",
    "no",
    "nu",
    "la",
    "le",
    "li",
    "lo",
    "lu",
  };

  std::vector<std::string> lastSillables[2] {
    {
      "lo",
      "lok",
      "man",
      "ron",
    },
    {
      "la",
      "ma",
      "ni",
      "na",
      "ne",
      "ra",
    }
  };

  std::string name;
  for( int i = 0; i < 1000; ++i ) {
    const bool threeSillables = 0 == (rand() % 3);
    name  = (firstSillables[rand() % firstSillables.size()])
      + ( threeSillables 
	  ? midSillables[rand() % midSillables.size()]
	  : std::string("") )
      + lastSillables[sex][rand() % lastSillables[sex].size()];
    auto it = names_.find(name);
    if( it == names_.end() ) {
      names_.insert(name);
      break;
    }
  }
  return name;
}

void NameGenerator::generateName(Entity* ent)
{
  auto name = ent->getComponent<components::Name>();
  if( !name ) {
    std::cerr << "ERROR: Entity does not contain name" << std::endl;
  }

  auto sex = ent->getComponent<components::Sex>();
  if( !sex ) {
    std::cerr << "ERROR: Entity does not contain sex" << std::endl;
  }

  name->name = generateName(sex->value);
}

void NameGenerator::addName(const std::string& name)
{
  names_.insert(name);
}

void NameGenerator::resetNames()
{
  names_.clear();
}
